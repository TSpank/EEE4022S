function H_gyro = H_gyro(thx,dthx,thy,dthy,thz,dthz)
%H_gyro
%    H_gyro = H_gyro(THX,DTHX,THY,DTHY,THZ,DTHZ)

%    This function was generated by the Symbolic Math Toolbox version 9.1.
%    26-Oct-2022 12:10:39

t2 = cos(thy);
t3 = sin(thy);
t4 = dthy.^2;
t5 = t3.^2;
t6 = t3.*t4;
t7 = t2.*t4;
t8 = t2.*t3.*(4.9e+1./5.0);
t9 = t5.*(4.9e+1./5.0);
t11 = t6+t8;
t10 = -t9;
t12 = t7+t10+4.9e+1./5.0;
H_gyro = [t2.*t11+t3.*t12;0.0;-t2.*t12+t3.*t11];
