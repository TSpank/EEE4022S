%time
Time.time_step = 0.01;
Time.t_end = 16.65;
Time.time_vector = 0:time_step:Time.t_end;
len = length(Time.time_vector);
nextguess = 0;


%get data
    %sensor 1
gyroReadings1 = gyroReadings1;
accelReadings1 = accelReadings1;
    %sensor 2
gyroReadings2 = gyroReadings2;
accelReadings2 = accelReadings2;

magReadings1 = magReadings1;
magReadings2 = magReadings2;

%store data
    %Sensor 1
Gyro_storage1.input = gyroReadings1;    
Gyro_storage1.States = zeros(length(Time.time_vector),6);
Gyro_storage1.States_predict = zeros(length(Time.time_vector),3);

Gyro_storage1.P = [];
Gyro_storage1.P_predict = [];

Gyro_storage1.F = [];
Gyro_storage1.z = zeros(length(Time.time_vector),3);
Gyro_storage1.time = zeros(length(Time.time_vector),1);
Gyro_storage1.accelmean = zeros(length(Time.time_vector),1);
Gyro_storage1.accelabs = zeros(length(Time.time_vector),1);
Gyro_storage1.hmmm = zeros(length(Time.time_vector),3);
Gyro_storage1.residual = zeros(length(Time.time_vector),3);
    %Sensor 2
Gyro_storage2.input = gyroReadings2; 
Gyro_storage2.States = zeros(length(Time.time_vector),6);
Gyro_storage2.States_predict = zeros(length(Time.time_vector),3);

Gyro_storage2.P = [];
Gyro_storage2.P_predict = [];

Gyro_storage2.F = [];
Gyro_storage2.z = zeros(length(Time.time_vector),3);
Gyro_storage2.time = zeros(length(Time.time_vector),1);
Gyro_storage2.accelmean = zeros(length(Time.time_vector),1);
Gyro_storage2.accelabs = zeros(length(Time.time_vector),1);
Gyro_storage2.hmmm = zeros(length(Time.time_vector),3);
Gyro_storage2.residual = zeros(length(Time.time_vector),3);

%set no sensors
no_sensors = 2;

for j = 1:no_sensors

%init
x_0 = [0;pi/4;0];
P_0 = [0; 0; 0];
P_0 = diag(P_0);
accelxmean = 0;

%storage
Storage_angles.States = zeros(length(Time.time_vector),6);
Storage_angles.States_predict = zeros(length(Time.time_vector),3);

Storage_angles.P = [];
Storage_angles.P_predict = [];

Storage_angles.F = [];
Storage_angles.z = zeros(length(Time.time_vector),3);
Storage_angles.time = zeros(length(Time.time_vector),1);
Storage_angles.accelmean = zeros(length(Time.time_vector),1);
Storage_angles.abs = zeros(length(Time.time_vector),1);
Storage_angles.hmmm = zeros(length(Time.time_vector),3);
Storage_angles.residual = zeros(length(Time.time_vector),3);


%get data
if (j == 1)
    gyroReadings = gyroReadings1;
    accelReadings = accelReadings1;
    magReadings = magReadings1;
end

if (j == 2)
    gyroReadings = gyroReadings2;
    accelReadings = accelReadings2;
    magReadings = magReadings2;
end

%initialize filter
xhat = x_0;
xhat_gyro = x_0;
P = P_0;

Storage_angles.States(1,:) = [xhat(1),gyroReadings(1,1),xhat(2),gyroReadings(1,2),xhat(3),gyroReadings(1,3)];
%%KALMAN FILTER
for i = 2:len
    mag_orientation = ecompass(magReadings(i,:),accelReadings(i,:));
    mag_angle = eulerd(mag_orientation,'XYZ','frame')*pi/180;
    
    %Process noise
    noise = [((pi*3.997)/180)^2 0 0];
    Q = diag(noise);

    %Predict
    xhat = F_gyro(xhat(1,1),gyroReadings(i,1),xhat(2,1),gyroReadings(i,2),xhat(3,1),gyroReadings(i,3),Time.time_step);
    xhat_gyro = F_gyro(xhat_gyro(1,1),gyroReadings(i,1),xhat_gyro(2,1),gyroReadings(i,2),xhat_gyro(3,1),gyroReadings(i,3),Time.time_step);
    V = dV_gyro(xhat_gyro(1,1),gyroReadings(i,1),xhat_gyro(2,1),gyroReadings(i,2),xhat_gyro(3,1),gyroReadings(i,3),Time.time_step);

    F = dF_gyro(xhat(1),xhat(2),xhat(3),0,0,0,Time.time_step);
    P = F*P*transpose(F) + V*Q*transpose(V);

    %storing predicted values
    
    Storage_angles.P_predict = cat(1,Storage_angles.P_predict,P);
    Storage_angles.F = cat(1,Storage_angles.F,F);

    %Get accel angles
%     if accelReadings(i,2)<0.25
%         ay = 0;
%     end
    
    z = [accelReadings(i,1);accelReadings(i,2);accelReadings(i,3)];

    if j ==1
    y = H_gyro(xhat(1,1),gyroReadings(i,1),xhat(2,1),gyroReadings(i,2),xhat(3,1),gyroReadings(i,3));
    H = dH_gyro(xhat(1),gyroReadings(i,1),xhat(2),gyroReadings(i,2),xhat(3),gyroReadings(i,3));
    end
    if j ==2
    y = H_gyro2(xhat(1,1),gyroReadings(i,1),xhat(2,1),gyroReadings(i,2),xhat(3,1),gyroReadings(i,3));
    H = dH_gyro2(xhat(1),gyroReadings(i,1),xhat(2),gyroReadings(i,2),xhat(3),gyroReadings(i,3));
    end
    Storage_angles.States_predict(i,:) = transpose(y);

    %Find kalman gain
    
    R = [0.0057^2;0.059^2;0.057^2];
    R = diag(R);
    K = P*transpose(H)/(H*P*transpose(H) + R);
    residual = z - y;
    hmmmmm = K*(residual);
    xhat = xhat + hmmmmm;
    P = (eye(3)-K*H)*P;


    
    Storage_angles.abs(i,1) = abs(accelReadings(i,1)-nextguess);
  
    %storing values
    Storage_angles.States(i,:) = [xhat(1),gyroReadings(i,1),xhat(2),gyroReadings(i,2),xhat(3),gyroReadings(i,3)];
    Storage_angles.P = cat(1,Storage_angles.P,P);
    Storage_angles.z(i,:) = transpose(z);
    Storage_angles.time(i) = Time.time_vector(i) ;
    Storage_angles.hmmm(i,:) =hmmmmm ;
    Storage_angles.residual(i,:) = residual;
end

if (j == 1)
    Gyro_storage1.States =  Storage_angles.States;
    Gyro_storage1.States_predict = Storage_angles.States_predict;
    
    Gyro_storage1.P = Storage_angles.P;
    Gyro_storage1.P_predict = Storage_angles.P_predict;

    Gyro_storage1.F = Storage_angles.F;
    Gyro_storage1.z = Storage_angles.z;
    Gyro_storage1.time = Storage_angles.time;
    Gyro_storage1.accelmean = Storage_angles.accelmean;
    Gyro_storage1.accelabs =  Storage_angles.abs;
    Gyro_storage1.hmmm = Storage_angles.hmmm;
    Gyro_storage1.residual = Storage_angles.residual;
end

if (j == 2)
    Gyro_storage2.States =  Storage_angles.States;
    Gyro_storage2.States_predict = Storage_angles.States_predict;
    
    Gyro_storage2.P = Storage_angles.P;
    Gyro_storage2.P_predict = Storage_angles.P_predict;

    Gyro_storage2.F = Storage_angles.F;
    Gyro_storage2.z = Storage_angles.z;
    Gyro_storage2.time = Storage_angles.time;
    Gyro_storage2.accelmean = Storage_angles.accelmean;
    Gyro_storage2.accelabs = Storage_angles.abs;
    Gyro_storage2.hmmm = Storage_angles.hmmm;
    Gyro_storage2.residual = Storage_angles.residual;
end

end





%%
stdarray = zeros(1666,1);
for i = 1:length(solution)
    stdarray(i) = Gyro_storage1.States(i,3) - solution(i,1);
end
std(stdarray)
%%


plot(time_vector,solution(:,1)*180/pi,'g');

grid on
plot(time_vector,Gyro_storage1.States(:,3)*180/pi,'b');
grid on
axis([0 18 -50 50])
plot(time_vector,Gyro_storage2.States(:,3)*180/pi,'r');
grid on
axis([0 18 -50 50])

hold off

plot(time_vector,zeros(length(solution(:,1))),'g');
hold on
grid on
plot(time_vector,Gyro_storage1.States(:,1)*180/pi,'b');
axis([0 18 -50 50])
plot(time_vector,Gyro_storage2.States(:,1)*180/pi,'r');
axis([0 18 -50 50])
hold off


plot(time_vector,zeros(length(solution(:,1))),'g');
hold on
grid on
plot(time_vector,Gyro_storage1.States(:,5)*180/pi,'b');
axis([0 18 -50 50])
plot(time_vector,Gyro_storage2.States(:,5)*180/pi,'r');
axis([0 18 -50 50])
hold off


plot(time_vector,Gyro_storage1.States(:,1)*180/pi,'r');
axis([0 18 -50 50])
plot(time_vector,Gyro_storage1.States(:,5)*180/pi,'r');
axis([0 18 -50 50])
disp('accel')
plot(time_vector,accelReadings1(:,1));
plot(time_vector,accelReadings2(:,1));

disp('KF solution')
plot(time_vector,solution(:,1)*180/pi,'g');

grid on
plot(time_vector,Gyro_storage2.States(:,1)*180/pi,'r');
axis([0 18 -50 50])
plot(time_vector,Gyro_storage2.States(:,3)*180/pi,'r');
plot(time_vector,Gyro_storage2.States(:,5)*180/pi,'r');
axis([0 18 -50 50])

plot(time_vector,Gyro_storage2.z(:,2)*180/pi,'r');

plot(time_vector,Gyro_storage1.accelabs,'b');
hold on
plot(time_vector,solution(:,1)*180/pi,'g');
hold off

plot(time_vector,Gyro_storage1.accelmean*180/pi,'b');
hold on
plot(time_vector,accelReadings1(:,1)*180/pi,'r');
hold off

%% RMS

rms(Gyro_storage1.States(:,1))*180/pi
rms(Gyro_storage1.States(:,3)-solution(:,1))*180/pi
rms(Gyro_storage1.States(:,5))*180/pi

rms(Gyro_storage2.States(:,1))*180/pi
rms(Gyro_storage2.States(:,3)-solution(:,1))*180/pi
rms(Gyro_storage2.States(:,5))*180/pi

rms(gyroAnglex1(:,1))*180/pi
rms(gyroAngley1(:,1)-solution(:,1))*180/pi
rms(gyroAnglez1(:,1))*180/pi

rms(gyroAnglex2(:,1))*180/pi
rms(gyroAngley2(:,1)-solution(:,1))*180/pi
rms(gyroAnglez2(:,1))*180/pi