%% .Fmatrices

%state transition atrix for pendulum
syms thz dthz thy dthy thx dthx;
syms L dt;

vars_gyro = [thz dthz thy dthy thx dthx dt];
states = [thz; thy; thx];
inputs = [dthz; dthy; dthx]

%% sensor 
thy = thy +dt*dthy;
thx = thx +dt*dthx;
thz = thz +dt*dthz;

F_gyro = [thz;thy;thx];
dF_gyro = jacobian(F_gyro, states);
dV_gyro = jacobian(F_gyro, inputs);
matlabFunction(F_gyro, 'file','C:\Users\user\Documents\MATLAB\Pendulum\Pendulum\\Accelgyrofusion_fixed\F_gyro','Vars', vars_gyro);
matlabFunction(dF_gyro, 'file','C:\Users\user\Documents\MATLAB\Pendulum\Pendulum\\Accelgyrofusion_fixed\dF_gyro','Vars', vars_gyro);
matlabFunction(dV_gyro, 'file','C:\Users\user\Documents\MATLAB\Pendulum\Pendulum\\Accelgyrofusion_fixed\dV_gyro','Vars', vars_gyro);