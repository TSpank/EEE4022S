function F_gyro = F_gyro(thz,dthz,thy,dthy,thx,dthx,dt)
%F_gyro
%    F_gyro = F_gyro(THZ,DTHZ,THY,DTHY,THX,DTHX,DT)

%    This function was generated by the Symbolic Math Toolbox version 9.1.
%    28-Oct-2022 07:21:45

F_gyro = [thz+dt.*dthz;thy+dt.*dthy;thx+dt.*dthx];
