function dth = Sim_pendulum(t,th)
    L = 1;
    g = 9.81;
    dth = [th(2,1);-(g/L)*sin(th(1,1))];
end