%%% DOUBLE PENDULUM
%% Measurement



%% Gyro

%ouputs angles from state vector
syms ax ay az;
syms thz dthz thy dthy thx dthx m1 m2 m3;
syms L dt;
vars_gyro = [thx dthx thy dthy thz dthz];
states = [thx; thy; thz];

%get ddthy
g = 9.8;
L = 1;
ddthy = (-g/L)*sin(thy);

%get acc in inertial frame
ax = L*ddthy*cos(thy) - L*dthy^2*sin(thy);
az = L*ddthy*sin(thy) + L*dthy^2*cos(thy);
ay = 0;


%get acc in sensor frame
ddx1 = -ax;
ddy1 = -ay;
ddz1 = -(az+9.8);

tosensor = rotationMatrix(0,-thy,0);
acc = tosensor*[ddx1;ddy1;ddz1];
ax = acc(1);
ay = acc(2);
az = acc(3);

%% sensor 
H_gyro = [ax;ay;az];

dh_gyro = jacobian(H_gyro,states);
matlabFunction(H_gyro, 'file','C:\Users\user\Documents\MATLAB\Pendulum\Pendulum\Accelgyrofusion_fixed\H_gyro','Vars', vars_gyro);
matlabFunction(dh_gyro, 'file','C:\Users\user\Documents\MATLAB\Pendulum\Pendulum\Accelgyrofusion_fixed\dH_gyro','Vars', vars_gyro);


%%

%ouputs angles from state vector
syms ax ay az;
syms thz dthz thy dthy thx dthx m1 m2 m3;
syms L dt;
vars_gyro = [thx dthx thy dthy thz dthz];
states = [thx; thy; thz];

%get ddthy
g = 9.8;

%get acc in inertial frame
ax = 0;
az = 0;
ay = 0;

%get acc in sensor frame
ddx1 = -ax;
ddy1 = -ay;
ddz1 = -(az+9.8);

tosensor = rotationMatrix(0,-thy,0);
acc = tosensor*[ddx1;ddy1;ddz1];
ax = acc(1);
ay = acc(2);
az = acc(3);

%% sensor 
H_gyro = [ax;ay;az];

dh_gyro = jacobian(H_gyro,states);
matlabFunction(H_gyro, 'file','C:\Users\user\Documents\MATLAB\Pendulum\Pendulum\Accelgyrofusion_fixed\H_gyro2','Vars', vars_gyro);
matlabFunction(dh_gyro, 'file','C:\Users\user\Documents\MATLAB\Pendulum\Pendulum\Accelgyrofusion_fixed\dH_gyro2','Vars', vars_gyro);