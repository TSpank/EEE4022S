filename_s3 = "C:\Users\user\Documents\STD\Sens 3\outfile.csv";
rawmeas.sensor3 = readmatrix(filename_s3);

filename_s4 = "C:\Users\user\Documents\STD\Sens 4\outfile.csv";
rawmeas.sensor4 = readmatrix(filename_s4);

filename_s6 = "C:\Users\user\Documents\STD\Sens 6\outfile.csv";
rawmeas.sensor6 = readmatrix(filename_s6);

end_point3 = length(rawmeas.sensor3(:,1));
end_point4 = length(rawmeas.sensor4(:,1));
end_point6 = length(rawmeas.sensor6(:,1));
%%
j =1;
i = 1;
while i < end_point3%length(rawmeas.sensor1(:,1))     
    if mod(rawmeas.sensor3(i,2)/1000,1)<0.01
        meas.sensor3(j,:) = rawmeas.sensor3(i,:);
        j = j + 1;
        i = i + 290;
    end
    i = i + 1;
end

j =1;
i = 1;
while i < end_point4%length(rawmeas.sensor1(:,1))     
    if mod(rawmeas.sensor4(i,2)/1000,1)<0.01
        meas.sensor4(j,:) = rawmeas.sensor4(i,:);
        j = j + 1;
        i = i + 290;
    end
    i = i + 1;
end

j =1;
i = 1;
while i < end_point6%length(rawmeas.sensor1(:,1))     
    if mod(rawmeas.sensor6(i,2)/1000,1)<0.01
        meas.sensor6(j,:) = rawmeas.sensor6(i,:);
        j = j + 1;
        i = i + 290;
    end
    i = i + 1;
end
%%
disp(['S3-----------------------------------------' ...
    '----------------------------------------------------'])
disp('accel')
%% 
% 

plot(meas.sensor3(:,4),'r')
plot(meas.sensor3(:,5),'g')
plot(meas.sensor3(:,6),'b')
disp('Gyro')
plot(meas.sensor3(:,9),'r')
plot(meas.sensor3(:,10),'g')
plot(meas.sensor3(:,11),'b')
disp('Mag')
plot(meas.sensor3(:,13),'r')
plot(meas.sensor3(:,14),'g')
plot(meas.sensor3(:,15),'b')

disp(['S4-----------------------------------------' ...
    '----------------------------------------------------'])
disp('accel')
plot(meas.sensor4(:,4),'r')
plot(meas.sensor4(:,5),'g')
plot(meas.sensor4(:,6),'b')
disp('Gyro')
plot(meas.sensor4(:,9),'r')
plot(meas.sensor4(:,10),'g')
plot(meas.sensor4(:,11),'b')
disp('Mag')
plot(meas.sensor4(:,13),'r')
plot(meas.sensor4(:,14),'g')
plot(meas.sensor4(:,15),'b')
disp(['S6-----------------------------------------' ...
    '----------------------------------------------------'])
disp('accel')
plot(meas.sensor6(:,4),'r')
plot(meas.sensor6(:,5),'g')
plot(meas.sensor6(:,6),'b')
disp('Gyro')
plot(meas.sensor6(:,9),'r')
plot(meas.sensor6(:,10),'g')
plot(meas.sensor6(:,11),'b')
disp('Mag')
plot(meas.sensor6(:,13),'r')
plot(meas.sensor6(:,14),'g')
plot(meas.sensor6(:,15),'b')

%% Mean

%avarage absolute bias accel
((abs(mean(meas.sensor3(200:500,4))) + abs(mean(meas.sensor4(3500:5500,4))) + abs(mean(meas.sensor6(2000:3000,4))))/3)/128
((abs(mean(meas.sensor3(200:500,5))) + abs(mean(meas.sensor4(3500:5500,5))) + abs(mean(meas.sensor6(2000:3000,5))))/3)/128
((abs(mean(meas.sensor3(200:500,6))) + abs(mean(meas.sensor4(3500:5500,6))) + abs(mean(meas.sensor6(2000:3000,6))))/3-128)/128

(abs(mean(meas.sensor3(200:500,9))) + abs(mean(meas.sensor4(3500:5500,9))) + abs(mean(meas.sensor6(2000:3000,9))))/3
(abs(mean(meas.sensor3(200:500,10))) + abs(mean(meas.sensor4(3500:5500,10))) + abs(mean(meas.sensor6(2000:3000,10))))/3
(abs(mean(meas.sensor3(200:500,11))) + abs(mean(meas.sensor4(3500:5500,11))) + abs(mean(meas.sensor6(2000:3000,11))))/3

(abs(mean(meas.sensor3(6000:7000,13))) + abs(mean(meas.sensor4(4000:4500,13))) + abs(mean(meas.sensor6(4000:4300,13))))/3
(abs(mean(meas.sensor3(6000:7000,14))) + abs(mean(meas.sensor4(4000:4500,14))) + abs(mean(meas.sensor6(4000:4300,14))))/3-12
(abs(mean(meas.sensor3(6000:7000,15))) + abs(mean(meas.sensor4(4000:4500,15))) + abs(mean(meas.sensor6(4000:4300,15))))/3-1020

%% STD


%accelration std
disp('accel')
std_x_s3 = std(meas.sensor3(200:500,4));
std_y_s3 = std(meas.sensor3(200:500,5));
std_z_s3 = std(meas.sensor3(200:500,6));

std_x_s4 = std(meas.sensor4(3500:5500,4));
std_y_s4 = std(meas.sensor4(3500:5500,5));
std_z_s4 = std(meas.sensor4(3500:5500,6));

std_x_s6 = std(meas.sensor6(2000:3000,4));
std_y_s6 = std(meas.sensor6(2000:3000,5));
std_z_s6 = std(meas.sensor6(2000:3000,6));

average_x = (std_x_s3+std_x_s4+std_x_s6)/3*(9.807/128)
average_y = (std_y_s3+std_y_s4+std_y_s6)/3*(9.807/128)
average_z = (std_z_s3+std_z_s4+std_z_s6)/3*(9.807/128)

%gyro
disp('Gyro')
std_x_s3 = std(meas.sensor3(200:500,9));
std_y_s3 = std(meas.sensor3(200:500,10));
std_z_s3 = std(meas.sensor3(200:500,11));

std_x_s4 = std(meas.sensor4(3500:5500,9));
std_y_s4 = std(meas.sensor4(3500:5500,10));
std_z_s4 = std(meas.sensor4(3500:5500,11));

std_x_s6 = std(meas.sensor6(2000:3000,9));
std_y_s6 = std(meas.sensor6(2000:3000,10));
std_z_s6 = std(meas.sensor6(2000:3000,11));

average_x_gyro = (std_x_s3+std_x_s4+std_x_s6)/3
average_y_gyro = (std_y_s3+std_y_s4+std_y_s6)/3
average_z_gyro = (std_z_s3+std_z_s4+std_z_s6)/3

%Magnetometer
disp('Mag')
std_x_s3 = std(meas.sensor3(200:500,13));
std_y_s3 = std(meas.sensor3(200:500,14));
std_z_s3 = std(meas.sensor3(200:500,15));

std_x_s4 = std(meas.sensor4(4500:5500,13));
std_y_s4 = std(meas.sensor4(4500:5500,14));
std_z_s4 = std(meas.sensor4(4500:5500,15));

std_x_s6 = std(meas.sensor6(2000:3000,13));
std_y_s6 = std(meas.sensor6(2000:3000,14));
std_z_s6 = std(meas.sensor6(2000:3000,15));

average_x_mag = (std_x_s3+std_x_s4+std_x_s6)/3
average_y_mag = (std_y_s3+std_y_s4+std_y_s6)/3
average_z_mag = (std_z_s3+std_z_s4+std_z_s6)/3