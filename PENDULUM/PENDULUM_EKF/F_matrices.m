%% Fmatrices
% 
%% Sensor 1

syms x1 y1 z1 dx1 dy1 dz1 ddx1 ddy1 ddz1...
     th1 dth1...
     alph1 dalph1...
     sig1 dsig1;
syms x2 y2 z2 dx2 dy2 dz2  ddx2 ddy2 ddz2...
     th2 dth2...
     alph2 dalph2...
     sig2 dsig2;
syms L dt;
vars_IMU1 = [x1 y1 z1 dx1 dy1 dz1 ddx1 ddy1 ddz1 th1 dth1 alph1 dalph1 sig1 dsig1...
             x2 y2 z2 dx2 dy2 dz2  ddx2 ddy2 ddz2 th2 dth2 alph2 dalph2 sig2 dsig2 L dt];

states_IMU1 = [x1; y1; z1; dx1; dy1; dz1; x2; y2; z2; dx2; dy2; dz2];
inputs_IMU1 = [ddx1; ddy1; ddz1; th1; alph1; sig1; ddx2; ddy2; ddz2; th2; alph2; sig2];

%get accelration into innertial frame 
    %sensor 1
    %this will have to change for actual sensors
toInertial_acc_s1 = rotationMatrix(0,th1,0);
acc_s1 = toInertial_acc_s1*[ddx1;ddy1;ddz1];
ddx1 = -acc_s1(1);
ddy1 = -acc_s1(2);
ddz1 = -(acc_s1(3)+g);

    %sensor 2
toInertial_acc_s2 = rotationMatrix(0,th2,0);
acc_s2 = toInertial_acc_s2*[ddx2;ddy2;ddz2];
ddx2 = -acc_s2(1);
ddy2 = -acc_s2(2);
ddz2 = -(acc_s2(3)+g);

%sensor1
    % update verlocities s1
dx1 = dx1 + ddx1*dt;
dy1 = dy1 + ddy1*dt;
dz1 = dz1 + ddz1*dt;
    % update positions s1
x1 = x1 + dx1*dt ;
y1 = y1 + dy1*dt ;
z1 = z1 + dz1*dt ;


    %assume constant accelaration model
    %accume constant angular verlocity
    %update angles

%sensor2
dx2 = dx2 + ddx2*dt;
dy2 = dy2 + ddy2*dt;
dz2 = dz2 + ddz2*dt;
    % update positions s2
x2 = x2 + dx2*dt;
y2 = y2 + dy2*dt;
z2 = z2 + dz2*dt;

    % update verlocities s2

    %assume constant accelaration model
    %accume constant angular verlocity
    %update angles
F_IMU1 = [x1; y1; z1; dx1; dy1; dz1; x2; y2; z2; dx2; dy2; dz2];

dF_F_IMU1 = jacobian(F_IMU1, states_IMU1);
dF_V_IMU1 = jacobian(F_IMU1, inputs_IMU1);
matlabFunction(F_IMU1, 'file','C:\Users\user\Documents\MATLAB\Pendulum\Pendulum\Pendulum_fixed\F_IMU1_0','Vars', vars_IMU1);
matlabFunction(dF_F_IMU1, 'file','C:\Users\user\Documents\MATLAB\Pendulum\Pendulum\Pendulum_fixed\dF_IMU1_0','Vars', vars_IMU1);
matlabFunction(dF_V_IMU1, 'file','C:\Users\user\Documents\MATLAB\Pendulum\Pendulum\Pendulum_fixed\dV_IMU1_0','Vars', vars_IMU1);
%% H matrix

syms x1 y1 z1 dx1 dy1 dz1 ddx1 ddy1 ddz1...
     th1 dth1...
     alph1 dalph1...
     sig1 dsig1;
syms x2 y2 z2 dx2 dy2 dz2  ddx2 ddy2 ddz2...
     th2 dth2...
     alph2 dalph2...
     sig2 dsig2;
H_vars = [x1 y1 z1 dx1 dy1 dz1 ddx1 ddy1 ddz1 th1 dth1 alph1 dalph1 sig1 dsig1...
             x2 y2 z2 dx2 dy2 dz2  ddx2 ddy2 ddz2 th2 dth2 alph2 dalph2 sig2 dsig2];

H_states = [x1; y1; z1; dx1; dy1; dz1; ddx1; x2; y2; z2; dx2; dy2; dz2];

H_out = [x1; y1; z1; dx1; dy1; dz1; ddx1; x2; y2; z2; dx2; dy2; dz2];

dH_out = jacobian(H_out, H_states);
matlabFunction(H_out, 'file','C:\Users\user\Documents\MATLAB\Pendulum\Pendulum\Pendulum_fixed\H_matrix','Vars', H_vars);
matlabFunction(dH_out, 'file','C:\Users\user\Documents\MATLAB\Pendulum\Pendulum\Pendulum_fixed\dH_matrix','Vars', H_vars);
%% Sensor 2

% clear;
% syms x1 y1 z1 dx1 dy1 dz1 ddx1 ddy1 ddz1...
%      th1 dth1...
%      alph1 dalph1...
%      sig1 dsig1;
% syms x2 y2 z2 dx2 dy2 dz2  ddx2 ddy2 ddz2...
%      th2 dth2...
%      alph2 dalph2...
%      sig2 dsig2;
% syms L dt;
% 
% vars_IMU2 = [x1 y1 z1 dx1 dy1 dz1 ddx1 ddy1 ddz1 th1 dth1 alph1 dalph1 sig1 dsig1...
%              x2 y2 z2 dx2 dy2 dz2  ddx2 ddy2 ddz2 th2 dth2 alph2 dalph2 sig2 dsig2 L dt];
% 
% states_IMU2 = [x1 y1 z1 dx1 dy1 dz1 ddx1 ddy1 ddz1 th1 dth1 alph1 dalph1 sig1 dsig1...
%              x2 y2 z2 dx2 dy2 dz2 ddx2 ddy2 ddz2 th2 dth2 alph2 dalph2 sig2 dsig2];
% 
% % update positions s2
% x2 = x2 + dx2*dt + 0.5*ddx2*dt^2;
% y2 = y2 + dy2*dt + 0.5*ddy2*dt^2;
% z2 = z2 + dz2*dt + 0.5*ddz2*dt^2;
% 
% % update verlocities s2
% dx2 = dx2 + ddx2*dt;
% dy2 = dy2 + ddy2*dt;
% dz2 = dz2 + ddz2*dt;
% 
% %using angles
% % update positions s1
% xyz_2 = [x2;y2;z2];
% toInertial_pos_2 = rotationMatrix(-(alph2+(pi/2)),-(th2+(pi/2)),0);
% xyz_2 = xyz_2 + toInertial_pos_2*transpose([L,0,0]);
% x1 = xyz_2(1);
% y1 = xyz_2(2);
% z1 = xyz_2(3);
% 
% % update verlocities s1
% xyz_v_2 = [dx2; dy2; dz2];
% toInertial_vel_2 = rotationMatrix(-(alph1+180),-(th1+180),0);
% xyz_v_2 = xyz_v_2 + toInertial_vel_2*transpose([dth2*L, dalph2*L, 0]);
% dx1 = xyz_v_2(1);
% dy1 = xyz_v_2(2);
% dz1 = xyz_v_2(3);
% 
% F_IMU2 = [x1 y1 z1 dx1 dy1 dz1 ddx1 ddy1 ddz1 th1 dth1 alph1 dalph1 sig1 dsig1...
%              x2 y2 z2 dx2 dy2 dz2  ddx2 ddy2 ddz2 th2 dth2 alph2 dalph2 sig2 dsig2];
% 
% dF_F_IMU2 = jacobian(F_IMU2, states_IMU2);
% matlabFunction(F_IMU2, 'file','C:\Users\user\Documents\MATLAB\Pendulum\Pendulum\F_IMU2_0','Vars', vars_IMU2);
% matlabFunction(dF_F_IMU2, 'file','C:\Users\user\Documents\MATLAB\Pendulum\Pendulum\dF_IMU2_0','Vars', vars_IMU2);
%