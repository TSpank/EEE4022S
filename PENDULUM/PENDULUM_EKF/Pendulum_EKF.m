%time
Time.time_step = 0.01;
Time.t_end = 16.65;
Time.time_vector = 0:Time.time_step:Time.t_end;
len = length(Time.time_vector);


%get data
    %gps data with noise
GPS1 = [x_pos+(0.013^2*randn(length(x_pos),1)),y_pos+(0.014^2*randn(length(x_pos),1)),z_pos+(0.009^2*randn(length(x_pos),1))];
GPS1_v = zeros(length(GPS1(:,1)),3);
GPS2 = [zeros(length(GPS1(:,1)),1)+(0.000017^2*randn(length(x_pos),1)),zeros(length(GPS1(:,1)),1)+(0.000021^2*randn(length(x_pos),1)),zeros(length(GPS1(:,1)),1)+(0.000393^2*randn(length(x_pos),1))];
GPS2_v = zeros(length(GPS1(:,1)),3);

    %gyro
gyro1 = Gyro_storage1.States;
gyro2 = Gyro_storage1.States;
    %accel
accel1 = accelReadings1;
accel2 = accelReadings2;

%init
toInertial_acc_s1 = rotationMatrix(gyro1(1,1),gyro1(1,3),gyro1(1,5));
acc_s1 = toInertial_acc_s1*[accel1(1,1);accel1(1,2);accel1(1,3)];
accx1_init = -acc_s1(1);
accy1_init = -acc_s1(2);
accz1_init = -(acc_s1(3)+g);

toInertial_acc_s2 = rotationMatrix(gyro2(1,1),gyro2(1,3),gyro2(1,5));
acc_s2 = toInertial_acc_s2*[accel2(1,1);accel2(1,2);accel2(1,3)];
accx2_init = -acc_s2(1);
accy2_init = -acc_s2(2);
accz2_init = -(acc_s2(3)+g);

%            x1         y1       z1   dx1  dy1   dz1              x2      y2      z2   dx2      dy2      dz2  
x_0 = [GPS1(1,1);GPS1(1,2);GPS1(1,3)   ;0   ;0    ;0 ; GPS2(1,1);GPS2(1,2);GPS2(1,3)    ;0       ;0       ;0 ];
P_0 = [0; 0; 0; 0; 0; 0;    0; 0; 0;   0; 0; 0];
P_0 = diag(P_0);
L = 1;
%%

%storage
Storage.States = zeros(length(Time.time_vector),12);
Storage.States_predict = zeros(length(Time.time_vector),12);

Storage.P = [];
Storage.P_predict = [];

Storage.F = [];
Storage.z = zeros(length(Time.time_vector),12);
Storage.z_sampled = zeros(length(Time.time_vector),6);
Storage.residual = zeros(length(Time.time_vector),12);

Storage.States(1,:) = x_0;
%initialize filter
xhat = x_0;
P = P_0;
residual = [0;0;0;0;0;0;0;0;0;0;0;0];
%%KALMAN FILTER
for i = 2:length(Time.time_vector)
   
  
    
    %Process noise                                                                                                                                                                         
                 %ddx1      ddy1      ddz1      th1                   alph1                    sig1                      ddx2        ddy2     ddz2           th2                    alph2                    sig2           
    noise = [0.057^2; 0.059^2;   0.057^2;    ((pi*0.0035)/180)^2;   ((pi*0.0035)/180)^2;    ((pi*0.0035)/180)^2;      0.057^2; 0.059^2;   0.057^2;    ((pi*0.0035)/180)^2;   ((pi*0.0035)/180)^2;    ((pi*0.0035)/180)^2;];
    Q = diag(noise);
    

    %sensor 1
    %Predict
                         %x1         y1        z1       dx1      dy1       dz1        ddx1         ddy1         ddz1        th1       dth1      alph1     dalph1       sig1      dsig1         x2         y2         z2        dx2        dy2        dz2          ddx2       ddy2       ddz2        th2      dth2       alph2     dalph2       sig2     dsig2
    xhat = F_IMU1_0(xhat(1,1),xhat(2,1),xhat(3,1),xhat(4,1),xhat(5,1),xhat(6,1),accel1(i,1) ,accel1(i,2) ,accel1(i,3),gyro1(i,3),gyro1(i,4) ,0,0,0,0                                       ,xhat(7,1),xhat(8,1),xhat(9,1),xhat(10,1),xhat(11,1),xhat(12,1),accel2(i,1) ,accel2(i,2) ,accel2(i,3),gyro2(i,3),gyro2(i,4) ,0,0,0,0,L,Time.time_step);
    F =   dF_IMU1_0(xhat(1,1),xhat(2,1),xhat(3,1),xhat(4,1),xhat(5,1),xhat(6,1),accel1(i,1) ,accel1(i,2) ,accel1(i,3),gyro1(i,3),gyro1(i,4) ,0,0,0,0                                       ,xhat(7,1),xhat(8,1),xhat(9,1),xhat(10,1),xhat(11,1),xhat(12,1),accel2(i,1) ,accel2(i,2) ,accel2(i,3),gyro2(i,3),gyro2(i,4) ,0,0,0,0,L,Time.time_step);
    V =   dV_IMU1_0(xhat(1,1),xhat(2,1),xhat(3,1),xhat(4,1),xhat(5,1),xhat(6,1),accel1(i,1) ,accel1(i,2) ,accel1(i,3),gyro1(i,3),gyro1(i,4) ,0,0,0,0                                       ,xhat(7,1),xhat(8,1),xhat(9,1),xhat(10,1),xhat(11,1),xhat(12,1),accel2(i,1) ,accel2(i,2) ,accel2(i,3),gyro2(i,3),gyro2(i,4) ,0,0,0,0,L,Time.time_step);
    P = F*P*transpose(F) + V*Q*transpose(V);

    %storing predicted values
    Storage.States_predict(i,:) = [xhat(1,1),xhat(2,1),xhat(3,1),xhat(4,1),xhat(5,1),xhat(6,1),xhat(7,1),xhat(8,1),xhat(9,1),xhat(10,1),xhat(11,1),xhat(12,1)];
    Storage.P_predict = cat(1,Storage.P_predict,P);
    Storage.F = cat(1,Storage.F,F);

    %read in data
    if i ==2
         z_gps = [GPS1(i,1),GPS1(i,2),GPS1(i,3),GPS2(i,1),GPS2(i,2),GPS2(i,3)];
         z = H_Sensor(GPS1(i,1), GPS1(i+1,1)  ,GPS1(i,2) ,GPS1(i+1,2) ,GPS1(i,3)  ,GPS1(i+1,3)   ,0       ,0      ,0   ,accel1(i,1) ,accel1(i,2) ,accel1(i,3) ,gyro1(i,3),gyro1(i,4) ,0,0,0,0 ,GPS2(i,1)       ,0         ,GPS2(i,2)       , 0    ,GPS2(i,3)    ,0          ,0       ,0      ,0       ,accel2(i,1) ,accel2(i,2) ,accel2(i,3) ,gyro2(i,3),gyro2(i,4) ,0,0,0,0,Time.time_step);
    end




    %set the sampling rate
    s_rate = 10;
  

    %sample GPS
    if mod(i,s_rate) == 0
                  %x1        x1_prev       y1    y1_prev        z1     z1_prev    dx1     dy1     dz1     ddx1             ddy1         ddz1         th1        dth1      alph1      dalph1        sig1    dsig1        x2     x2_prev       y2 y2_prev        z2 z2_prev      dx2      dy2      dz2           ddx2         ddy2         ddz2         th2       dth2        alph2     dalph2     sig2    dsig2
        z = H_Sensor(GPS1(i,1),GPS1(i,1),GPS1(i,2),GPS1(i,2) ,GPS1(i,3) ,GPS1(i,3)    ,0       ,0      ,0   ,accel1(i,1) ,accel1(i,2) ,accel1(i,3) ,gyro1(i,3),gyro1(i,4) ,0,0,0,0 ,GPS2(i,1)     ,0      ,GPS2(i,2) ,0      ,GPS2(i,3)     ,0       ,0       ,0      ,0       ,accel2(i,1) ,accel2(i,2) ,accel2(i,3) ,gyro2(i,3),gyro2(i,4) ,0,0,0,0,Time.time_step);
   
    if i > 3   %so that verolocity can be calculated
                 %x1         x1_prev        y1       y1_prev       z1        z1_prev         dx1     dy1     dz1     ddx1             ddy1         ddz1         th1        dth1      alph1      dalph1        sig1    dsig1        x2       x2_prev               y2   y2_prev         z2    z2_prev      dx2      dy2     dz2           ddx2         ddy2         ddz2         th2       dth2        alph2     dalph2     sig2    dsig2
        z = H_Sensor(GPS1(i,1), GPS1(i-1,1)  ,GPS1(i,2) ,GPS1(i-1,2) ,GPS1(i,3)  ,GPS1(i-1,3)   ,0       ,0      ,0   ,accel1(i,1) ,accel1(i,2) ,accel1(i,3) ,gyro1(i,3),gyro1(i,4) ,0,0,0,0 ,GPS2(i,1)       ,0         ,GPS2(i,2)       , 0    ,GPS2(i,3)    ,0          ,0       ,0      ,0       ,accel2(i,1) ,accel2(i,2) ,accel2(i,3) ,gyro2(i,3),gyro2(i,4) ,0,0,0,0,Time.time_step);
    end
    end





    %Find kalman gain
                        %x1       y1        z1       dx1      dy1       dz1      ddx1      ddy1      ddz1        th1       dth1      alph1     dalph1       sig1      dsig1          x2        y2          z2        dx2        dy2        dz2       ddx2       ddy2       ddz2        th2      dth2       alph2     dalph2       sig2     dsig2
    H = [1 1 1 1 1 1 1 1 1 1 1 1];%dH_matrix(xhat(1,1),xhat(2,1),xhat(3,1),xhat(4,1),xhat(5,1),xhat(6,1),xhat(7,1),xhat(8,1),xhat(9,1),xhat(10,1),xhat(11,1),xhat(12,1),xhat(13,1),xhat(14,1),xhat(15,1),xhat(16,1),xhat(17,1),xhat(18,1),xhat(19,1),xhat(20,1),xhat(21,1),xhat(22,1),xhat(23,1),xhat(24,1),xhat(25,1),xhat(26,1),xhat(27,1),xhat(28,1),xhat(29,1),xhat(30,1));
    H = diag(H);
    R = [0  0  0  0  0  0  0  0  0  0  0  0  ;...   %x1
         0  0  0  0  0  0  0  0  0  0  0  0  ;...   %y1
         0  0  0  0  0  0  0  0  0  0  0  0  ;...   %z1
         0  0  0  0  0  0  0  0  0  0  0  0  ;...   %dx1
         0  0  0  0  0  0  0  0  0  0  0  0  ;...   %dy1
         0  0  0  0  0  0  0  0  0  0  0  0  ;...   %dz1
         0  0  0  0  0  0  0  0  0  0  0  0  ;...   %x2
         0  0  0  0  0  0  0  0  0  0  0  0  ;...   %y2
         0  0  0  0  0  0  0  0  0  0  0  0  ;...   %z2
         0  0  0  0  0  0  0  0  0  0  0  0  ;...   %dx2
         0  0  0  0  0  0  0  0  0  0  0  0  ;...   %dy2
         0  0  0  0  0  0  0  0  0  0  0  0  ];    %dz2

    R(1,1) = 0.013^2;  R(7,7) = 0.013^2;  %X1 and X2
    R(2,2) = 0.014^2; R(8,8) = 0.014^2; %Y1 and Y2
    R(3,3) = 0.009^2; R(9,9) = 0.009^2; %Z1 and Z2

    dx_std = sqrt(2)*0.013/(Time.time_step*s_rate);
    R(4,4) = dx_std^2;      %dX1
    R(10,10) = dx_std^2;    %dX2

    dy_std = sqrt(2)*0.014/(Time.time_step*s_rate);
    R(5,5) = dy_std^2;      %dY1
    R(11,11) = dy_std^2;    %dY2

    dz_std = sqrt(2)*0.009/(Time.time_step*s_rate);
    R(6,6) = dz_std^2;      %dZ1
    R(12,12) = dz_std^2;   R=R*0.0001; %dZ2







    %kalman gain
    K = (P*transpose(H))/(H*P*transpose(H) + R);

    


    
    %Update
    residual = z - xhat;
    xhat = xhat + K*(residual);
    P = (eye(12)-K*H)*P;
    
    if mod(i,s_rate) == 0 
    thold = 0.2;
    z_gps = [GPS1(i,1),GPS1(i,2),GPS1(i,3),GPS2(i,1),GPS2(i,2),GPS2(i,3)];
    

    end

    %simultion stopped at 0 without decelrating, this results in a verlocity
    %drift that would not occur in real word testing, this simulation error
    %was accounted for by setting the verlocity back to zero at the
    %time 10


    %storing values
    Storage.States(i,:) = [xhat(1,1),xhat(2,1),xhat(3,1),xhat(4,1),xhat(5,1),xhat(6,1),xhat(7,1),xhat(8,1),xhat(9,1),xhat(10,1),xhat(11,1),xhat(12,1)];
    Storage.P = cat(1,Storage.P,P);
    Storage.z(i,:) = transpose(z);
    Storage.risidual(i,:) = transpose(residual);
    Storage.z_sampled(i,:) = z_gps;
    
end

%%
colnames = {'x1','y1','z1','dx1','dy1','dz1',...
            'x2','y2','z2','dx2','dy2','dz2'};

colnames2 = {'x1','y1','z1',...
            'x2','y2','z2'};


Table_States = array2table(Storage.States,'VariableNames',colnames);
Table_zsam = array2table(Storage.z_sampled,'VariableNames',colnames2);
Table_z = array2table(Storage.z,'VariableNames',colnames);
Table_predict = array2table(Storage.States_predict,'VariableNames',colnames);
Table_residual = array2table(Storage.risidual,'VariableNames',colnames);
%%

plot(Time.time_vector(:),x_pos(:,1),'g')
hold on
axis([0 18 -1 1])
plot(Time.time_vector(:),Storage.States(:,1))
grid on
plot(Time.time_vector(:),Storage.States(:,7))
hold off
plot(Time.time_vector(:),y_pos(:,1),'g')
axis([0 18 -1 1])
hold on
plot(Time.time_vector(:),Storage.States(:,2))
grid on
axis([0 18 -1 1])
plot(Time.time_vector(:),Storage.States(:,8))
axis([0 18 -1 1])
hold off

plot(Time.time_vector(:),z_pos(:,1),'g')
hold on
plot(Time.time_vector(:),Storage.States(:,3))
grid on
plot(Time.time_vector(:),Storage.States(:,9))
hold off
plot(Time.time_vector(:),Storage.z_sampled(:,1))
hold on
plot(Time.time_vector(:),Storage.z_sampled(:,4),'r')
grid on
hold off
plot(Time.time_vector(:),Storage.z_sampled(:,2))
axis([0 18 -1 1])
hold on
plot(Time.time_vector(:),Storage.z_sampled(:,5),'r')
grid on
axis([0 18 -1 1])
hold off
plot(Time.time_vector(:),Storage.z_sampled(:,3))
hold on
grid on
plot(Time.time_vector(:),Storage.z_sampled(:,6),'r')
hold off

%% Smoothing

smooth = 10;
%xdirection1
smoothx_s1 = smoothdata(Storage.States(:,1),'sgolay',smooth);
smooth = 1000000;
smoothx_s2 = smoothdata(Storage.States(:,7),'sgolay',smooth);
plot(Time.time_vector(:),x_pos(:,1),'g')
hold on
grid on
plot(Time.time_vector(:),smoothx_s1(:,1),'k')
plot(Time.time_vector(:),smoothx_s2(:,1),'m')
hold off

smooth = 100
%ydirection1
smoothy_s1 = smoothdata(Storage.States(:,2),'sgolay',smooth);
smoothy_s2 = smoothdata(Storage.States(:,8),'sgolay',smooth);
plot(Time.time_vector(:),y_pos(:,1),'g')
hold on
plot(Time.time_vector(:),smoothy_s1(:,1),'k')
grid on
axis([0 18 -1 1])
plot(Time.time_vector(:),smoothy_s2(:,1),'m')
hold off

smooth = 50;
%zdirection1
smoothz_s1 = smoothdata(Storage.States(:,3),'sgolay',smooth);
smooth = 100;
plot(Time.time_vector(:),z_pos(:,1),'g')
smoothz_s2 = smoothdata(Storage.States(:,9),'sgolay',smooth);
hold on
plot(Time.time_vector(:),y_pos(:,1),'g')
grid on
plot(Time.time_vector(:),smoothz_s2(:,1),'m')
plot(Time.time_vector(:),smoothz_s1(:,1),'k')
hold off
%% RMS

%Sensor 1
        %GPS

rms(x_pos(:,1)-Storage.z_sampled(:,1))
rms(y_pos(:,1)-Storage.z_sampled(:,2))
rms(z_pos(:,1)-Storage.z_sampled(:,3))

corrcoef(x_pos(:,1),Storage.z_sampled(:,1))
        %fusion


rms(x_pos(:,1)-Storage.States(:,1))
rms(y_pos(:,1)-Storage.States(:,2))
rms(z_pos(:,1)-Storage.States(:,3))



%-------------------------------------------------------------------------------->

%Sensor 2
        %GPS
rms(Storage.z_sampled(:,4))
rms(Storage.z_sampled(:,5))
rms(Storage.z_sampled(:,6))
        %fusion
rms(Storage.States(:,7))
rms(Storage.States(:,8))
rms(Storage.States(:,9))


disp('smoothed')
%smoothed

%Sensor 1
        %fusion
rms(x_pos(:,1)-smoothx_s1(:,1))
rms(y_pos(:,1)-smoothy_s1(:,1))
rms(z_pos(:,1)-smoothz_s1(:,1))

%-------------------------------------------------------------------------------->

%Sensor 2
        %fusion
rms(smoothx_s2(:,1))
rms(smoothy_s2(:,1))
rms(smoothz_s2(:,1))