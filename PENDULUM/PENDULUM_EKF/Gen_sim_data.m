clear;
time_step = 0.01;
t_end = 9.91;
time_vector = 0:time_step:t_end; 
time_vector2 = t_end:time_step:16.64;
g = 9.8;
L = 1;

%% sensor 1 

th_0 = [pi/4;0];
[time_vector,solution]=ode45(@Sim_pendulum,time_vector,th_0);
 
for i = 1:length(time_vector)
     orientation(i) = quaternion([0,solution(i,1),0],'euler','XYZ','frame');
end

solution2 = zeros(length(time_vector2),2);
for i = 1:length(time_vector2)
     orientation2(i) = quaternion([0,0,0],'euler','XYZ','frame');
     solution2(i,:) = [0,0];
end
orientation = cat(1,transpose(orientation),transpose(orientation2));
time_vector = cat(1,time_vector,transpose(time_vector2));
solution = cat(1,solution,solution2);

hold on

%theta
th1 = solution(:,1);
disp("th plot")
plot(time_vector,solution(:,1)*180/pi,'r');

%theta_dot
dalph1 = zeros(length(time_vector),1);
dsigma1 = zeros(length(time_vector),1);
dth1 = solution(:,2);
disp("th_dot plot");
plot(time_vector,solution(:,2),'b');

%theta_ddot
ddth1 = (-g/L)*sin(solution(:,1));
disp("th_ddot plot");
plot(time_vector,(-g/L)*sin(solution(:,1)),'g');

hold off

%xy accel
x_accel = zeros(length(time_vector),1);
z_accel = zeros(length(time_vector),1);
y_accel = zeros(length(time_vector),1);

x_pos = zeros(length(time_vector),1);
z_pos = zeros(length(time_vector),1);
y_pos = zeros(length(time_vector),1);

for i = 1:length(time_vector)
    x_accel(i) = L*ddth1(i)*cos(th1(i)) - L*dth1(i)^2*sin(th1(i));
    z_accel(i) = L*ddth1(i)*sin(th1(i)) + L*dth1(i)^2*cos(th1(i));
    y_accel(i) = 0;

    x_pos(i) = L*sin(th1(i));
    z_pos(i) = -L*(cos(th1(i)));
    y_pos(i) = 0;
    
end

disp('x and z pos')
plot(time_vector,x_pos(:,1),'r');
hold on
plot(time_vector,z_pos(:,1),'b');
hold off

disp('x and z accel')
plot(time_vector,x_accel(:,1),'r');
hold on
plot(time_vector,z_accel(:,1),'b');
plot(time_vector,solution(:,1)*180/pi,'g');
hold off

%% Sensor 2

%xy accel
x_accel2 = zeros(length(time_vector),1);
z_accel2 = zeros(length(time_vector),1);
y_accel2 = zeros(length(time_vector),1);
%% CreateIMU sensor 1

acc = [x_accel,y_accel,z_accel];
angVel = [dsigma1,dth1,dalph1];
%IMU
IMU  = imuSensor('accel-gyro-mag','ReferenceFrame',"ENU");

IMU.Gyroscope.ConstantBias = [0.028 0.047 0.0257];
IMU.Gyroscope.BiasInstability = [0 0 0];
IMU.Gyroscope.RandomWalk = [0 0 0];
IMU.Gyroscope.NoiseDensity = [(pi*3.997)/(7*180) (pi*4.867)/(7*180) (pi*6.536)/(7*180)];
IMU.Accelerometer.NoiseDensity = [0.057/7 0.059/7 0.057/7];
IMU.Accelerometer.ConstantBias = [0.028/7 0.047/7 0.029/7];
IMU.Magnetometer.NoiseDensity = [0 1.127/7 5.773/7];
IMU.Magnetometer.ConstantBias = [2.042 6.445 18.403];
[accelReadings1,gyroReadings1,magReadings1] = IMU(acc,angVel,orientation);

%IMU accel plots
ddth1 = (-g/L)*solution(:,2);
disp("IMU accel");

plot(time_vector,accelReadings1(:,1),'r');
hold on
plot(time_vector,accelReadings1(:,3),'b');
plot(time_vector,solution(:,1)*180/pi,'g');
hold off

%% CreateIMU sensor 2

acc2 = [x_accel2,y_accel2,z_accel2];
angVel2 = [dsigma1,dth1,dalph1];

for i = 1:1666
    angVel2(i,:)=[0,0,0];
    orientation2_still(i) = quaternion([0,0,0],'euler','XYZ','frame');

end
%IMU
IMU2  = imuSensor('accel-gyro-mag','ReferenceFrame',"ENU");

IMU2.Gyroscope.ConstantBias = [0.028 0.047 0.0257];
IMU2.Gyroscope.BiasInstability = [0 0 0];
IMU2.Gyroscope.RandomWalk = [0 0 0];
IMU2.Gyroscope.NoiseDensity = [(pi*3.997)/(7*180) (pi*4.867)/(7*180) (pi*6.536)/(7*180)];
IMU2.Accelerometer.NoiseDensity = [0.057/7 0.059/7 0.057/7];
IMU2.Accelerometer.ConstantBias = [0.028/7 0.047/7 0.029/7];
IMU2.Magnetometer.NoiseDensity = [0 1.127/7 5.773/7];
IMU2.Magnetometer.ConstantBias = [2.042 6.445 18.403];
[accelReadings2,gyroReadings2,magReadings2] = IMU2(acc2,angVel2,transpose(orientation2_still));

%IMU accel plots
ddth1 = (-g/L)*solution(:,2);
disp("IMU accel 2");

plot(time_vector,accelReadings2(:,1),'r');
hold on
plot(time_vector,accelReadings2(:,3),'b');
plot(time_vector,solution(:,1)*180/pi,'g');
hold off
%% Gyro readings

gyroAngley1 = zeros(length(time_vector),1);
gyroAngley1(1) = th_0(1);
gyroAnglex1 = zeros(length(time_vector),1);
gyroAnglex1(1) = 0;
gyroAnglez1 = zeros(length(time_vector),1);
gyroAnglez1(1) = 0;

gyroAngley2 = zeros(length(time_vector),1);
gyroAngley2(1) = 0;
gyroAnglex2 = zeros(length(time_vector),1);
gyroAnglex2(1) = 0;
gyroAnglez2 = zeros(length(time_vector),1);
gyroAnglez2(1) = 0;

%Plot and obtain Gyro values
for i = 2:length(time_vector)
    gyroAnglex1(i) = gyroAnglex1(i-1) + gyroReadings1(i,1)*time_step; 
    gyroAngley1(i) = gyroAngley1(i-1) + gyroReadings1(i,2)*time_step; 
    gyroAnglez1(i) = gyroAnglez1(i-1) + gyroReadings1(i,3)*time_step; 

    gyroAnglex2(i) = gyroAnglex2(i-1) + gyroReadings2(i,1)*time_step; 
    gyroAngley2(i) = gyroAngley2(i-1) + gyroReadings2(i,2)*time_step; 
    gyroAnglez2(i) = gyroAnglez2(i-1) + gyroReadings2(i,3)*time_step; 
end

disp('gyro angle')
grid on
plot(time_vector,gyroAngley1(:,1)*180/pi,'b');
grid on
hold on
plot(time_vector,gyroAngley2(:,1)*180/pi,'r');
plot(time_vector,solution(:,1)*180/pi,'g');
hold off

disp('gyro angle')
grid on
plot(time_vector,gyroAnglex1(:,1)*180/pi,'b');
hold on
plot(time_vector,gyroAnglex2(:,1)*180/pi,'r');
grid on
plot(time_vector,zeros(length(gyroAnglex1(:,1))),'g');
hold off

disp('gyro angle')
grid on
plot(time_vector,gyroAnglez1(:,1)*180/pi,'b');
hold on
plot(time_vector,gyroAnglez2(:,1)*180/pi,'r');
grid on
plot(time_vector,zeros(length(gyroAnglex1(:,1))),'g');
hold off

acc_angle = zeros(length(time_vector),1);
acc_angle2 = zeros(length(time_vector),1);

%% Accel Readings

%pitch
for i = 1:length(time_vector)
    acc_angle(i) = atan2(accelReadings1(i,1),9.8); 
end

disp('acc angle')
plot(time_vector,solution(:,1)*180/pi,'g');
hold on 
grid on
plot(time_vector,acc_angle(:,1)*180/pi,'b');
hold off

%pitch
for i = 1:length(time_vector)
    acc_angle2(i) = atan2(accelReadings2(i,1),9.8); 
    
end

disp('acc angle')
%plot(time_vector,solution(:,1)*180/pi,'g');

grid on




hold on
plot(time_vector,acc_angle2(:,1)*180/pi,'r');
hold off


%% Get accelaromter back into intertial frame

accReading_interial1 = zeros(length(time_vector),3);
accReading_interial2 = zeros(length(time_vector),3);
accReading_interial1_nograv = zeros(length(time_vector),3);
accReading_interial2_nograv = zeros(length(time_vector),3);
accReading_interial1_nograv222 = zeros(length(time_vector),3);
accReading_interial2_nograv222 = zeros(length(time_vector),3);
velocityx = zeros(length(time_vector),1);
positionx = zeros(length(time_vector),1);
v_usingposition = zeros(length(time_vector),1);
positionx(1,1) = 0.707;


wierdtestacc = zeros(length(time_vector),3);

for i = 1:length(time_vector)
    toInertial = quaternion([0,-solution(i,1),0],'euler','XYZ','frame');
    accReading_interial1(i,:) = rotateframe(toInertial,accelReadings1(i,:)); 
    accReading_interial2(i,:) = rotateframe(toInertial,accelReadings2(i,:)); 
    
    accReading_interial1_nograv(i,:) =  [accReading_interial1(i,1),accReading_interial1(i,2),(accReading_interial1(i,3))];
    accReading_interial2_nograv(i,:) =  [accReading_interial2(i,1),accReading_interial2(i,2),(accReading_interial2(i,3))];
end

for i = 1:1666
    toInertial_acc1 = rotationMatrix(0,solution(i,1),0);
    [accelReadings1(i,1),accelReadings1(i,2),accelReadings1(i,3)];
    acc1 = toInertial_acc1*transpose([accelReadings1(i,1),accelReadings1(i,2),accelReadings1(i,3)]);
    ddx1 = -acc1(1);
    ddy1 = -acc1(2);
    ddz1 = -(acc1(3)+9.8);


    ddx = -ddx1;
    ddy = -ddy1;
    ddz = -(ddz1+9.8);

    tosensor = rotationMatrix(0,-solution(i,1),0);
    acc = tosensor*[ddx;ddy;ddz];
    ax = acc(1);
    ay = acc(2);
    az = acc(3);
    wierdtestacc(i,:) = [ax,ay,az];

    if i>1
    velocityx(i) = velocityx(i-1) + ddx1*0.01;
    positionx(i) = positionx(i-1) + velocityx(i-1)*0.01 + 0.5*ddx1*(0.01^2);
    v_usingposition(i) = (x_pos(i)-x_pos(i-1))/0.01;
    end
    accReading_interial1_nograv222(i,:) = [ddx1,ddy1,ddz1];

    acc2 = toInertial_acc1*transpose([accelReadings2(i,1),accelReadings2(i,2),accelReadings2(i,3)]);
    ddx2 = -acc2(1);
    ddy2 = -acc2(2);
    ddz2 = -acc2(3);
    accReading_interial2_nograv222(i,:) = [ddx2,ddy2,ddz2];
end
%%

plot(time_vector,accReading_interial1_nograv222(:,1))
hold on
plot(time_vector,solution(:,1))
hold off

plot(time_vector,x_pos(:,1))
hold on
plot(time_vector(1:1000),velocityx(1:1000,1),'r')
plot(time_vector(1:1000),v_usingposition(1:1000,1))
%plot(time_vector(1:1000),Storage.z(1:1000,4))
hold off

plot(time_vector(:,1),x_pos(:,1))
hold on
plot(time_vector(:,1),positionx(:,1))

hold off
%% Magnetometer data

mag_orientation = ecompass(magReadings1,accelReadings1);
mag_angles = eulerd(mag_orientation,'XYZ','frame');

mag_orientation2 = ecompass(magReadings2,accelReadings2);
mag_angles2 = eulerd(mag_orientation2,'XYZ','frame');