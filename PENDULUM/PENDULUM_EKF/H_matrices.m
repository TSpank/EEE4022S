%%% DOUBLE PENDULUM
%% Measurement

%ouputs angles from state vector
syms x1 y1 z1 dx1 dy1 dz1 ddx1 ddy1 ddz1...
     x1_prev y1_prev z1_prev...
     th1 dth1...
     alph1 dalph1...
     sig1 dsig1;
syms x2 y2 z2 dx2 dy2 dz2  ddx2 ddy2 ddz2...
     x2_prev y2_prev z2_prev...
     th2 dth2...
     alph2 dalph2...
     sig2 dsig2;
syms L dt;
g=9.8;
vars_sensor = [x1 x1_prev y1 y1_prev z1 z1_prev dx1 dy1 dz1 ddx1 ddy1 ddz1 th1 dth1 alph1 dalph1 sig1 dsig1...
               x2 x2_prev y2 y2_prev z2 z2_prev dx2 dy2 dz2 ddx2 ddy2 ddz2 th2 dth2 alph2 dalph2 sig2 dsig2 dt];

states = [x1; y1; z1; dx1; dy1; dz1; x2; y2; z2; dx2; dy2; dz2];

%find verlocities
    %sensor 1
dx1 = (x1 - x1_prev)/dt;
dy1 = (y1 - y1_prev)/dt;
dz1 = (z1 - z1_prev)/dt;
    %sensor 2
dx2 = (x2 - x2_prev)/dt;
dy2 = (y2 - y2_prev)/dt;
dz2 = (z2 - z2_prev)/dt;

%get accelration into innertial frame 
    %sensor 1
    %this will have to change for actual sensors
toInertial_acc_s1 = rotationMatrix(0,th1,0);
acc_s1 = toInertial_acc_s1*[ddx1;ddy1;ddz1];
ddx1 = -acc_s1(1);
ddy1 = -acc_s1(2);
ddz1 = -(acc_s1(3)+g);

    %sensor 2
toInertial_acc_s2 = rotationMatrix(0,th2,0);
acc_s2 = toInertial_acc_s2*[ddx2;ddy2;ddz2];
ddx2 = -acc_s2(1);
ddy2 = -acc_s2(2);
ddz2 = -(acc_s2(3)+g);

%% sensor 
H_sensor = [x1; y1; z1; dx1; dy1; dz1; x2; y2; z2; dx2; dy2; dz2];
dh_sensor = jacobian(H_sensor,states);
matlabFunction(H_sensor, 'file','C:\Users\user\Documents\MATLAB\Pendulum\Pendulum\Pendulum_fixed\H_Sensor','Vars', vars_sensor);
matlabFunction(dh_sensor, 'file','C:\Users\user\Documents\MATLAB\Pendulum\Pendulum\Pendulum_fixed\dH_Sensor','Vars', vars_sensor);