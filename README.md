# EEE4022S

Extended Kalman filter used to predict the movement of GPS sensors in a kinematic chain. These files include both simulation files and the algorithm implemented in the real world test. 

The pendulum was a simple test of the system using two GPS sensors. One was attached ad the top of the pendulum, while the second at the bottom. This was done completely in simulation. 

The GPS_EKF is the algorithm which was implemented as a motion capture system; which is able to capture an athletes movement over extremely large areas.
