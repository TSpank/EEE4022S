%time
Time.time_step = 0.01;
Time.t_end = 10;
Time.time_vector = 0:Time.time_step:Time.t_end;
len = length(Time.time_vector)+1;

%for plotting
tstart = 30;
tend = 300;

%get data
    %gps
GPS1 = hip;
GPS2 = knee;
GPS3 = ankle;

%            x1         y1       z1    dx1   dy1   dz1           x2        y2        z2   dx2   dy2   dz2           x3        y3       z3    dx3   dy3   dz3
x_0 = [GPS1(1,1);GPS1(1,2);GPS1(1,3)    ;0    ;0    ;0   ;GPS2(1,1);GPS2(1,2);GPS2(1,3)    ;0    ;0    ;0   ;GPS3(1,1);GPS3(1,2);GPS3(1,3)    ;0    ;0    ;0];
P_0 = [0        ;0        ;0            ;0    ;0    ;0   ;0        ;0        ;0            ;0    ;0    ;0   ;0        ;0        ;0            ;0    ;0    ;0];
P_0 = diag(P_0);
%P_0 =eye(18);

plot3(GPS1(tstart:end,1),GPS1(tstart:end,2),GPS1(tstart:end,3));
hold on
grid on
plot3(GPS2(tstart:end,1),GPS2(tstart:end,2),GPS2(tstart:end,3));

plot3(GPS3(tstart:end,1),GPS3(tstart:end,2),GPS3(tstart:end,3));
hold off

x = GPS1;
y = GPS2;
z = GPS3;

Q1 = 0;
Q2 = 0;
Q3 = 0;
%%
%storage
Storage.States = zeros(length(Time.time_vector),18);
Storage.States_predict = zeros(length(Time.time_vector),18);
Storage.K = zeros(length(Time.time_vector),18);

Storage.P = diag([0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0]);
Storage.P_predict = diag([0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0]);

Storage.F = diag([0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0]);
Storage.z = zeros(length(Time.time_vector),18);
Storage.z_sampled = zeros(length(Time.time_vector),9);
Storage.residual = zeros(length(Time.time_vector),18);
Storage.Q = eye(18);

Storage.States(1,:) = x_0;
%initialize filter
xhat = x_0;
P = P_0;
residual = [0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0];


    %set the sampling rate
    s_rate = 50;


%%KALMAN FILTER
for i = 2:328
    %Process noise                                                                                                                                                                         
%                   x1         y1          z1                             dx1                                dy1                             dz1        
    noise = [0.0172^2;   0.0211^2;  0.0393^2;   0.0172^2*Time.time_step*s_rate;   0.0211^2*Time.time_step*s_rate;  0.0393^2*Time.time_step*s_rate;...
             0.0172^2;   0.0211^2;  0.0393^2;   0.0172^2*Time.time_step*s_rate;   0.0211^2*Time.time_step*s_rate;  0.0393^2*Time.time_step*s_rate;...
             0.0172^2;   0.0211^2;  0.0393^2;   0.0172^2*Time.time_step*s_rate;   0.0211^2*Time.time_step*s_rate;  0.0393^2*Time.time_step*s_rate];
    Q = diag(noise);

    %sensor 1
    %Predict
    %                   x1       y1        z1       dx1       dy1       dz1        x2        y2        z2        dx2        dy2        dz2         x3         y3         z3        dx3        dy3        dz3
    xhat = F_GPS(xhat(1,1),xhat(2,1),xhat(3,1),xhat(4,1),xhat(5,1),xhat(6,1),xhat(7,1),xhat(8,1),xhat(9,1),xhat(10,1),xhat(11,1),xhat(12,1),xhat(13,1),xhat(14,1),xhat(15,1),xhat(16,1),xhat(17,1),xhat(18,1),Time.time_step);
    F =   dF_GPS(xhat(1,1),xhat(2,1),xhat(3,1),xhat(4,1),xhat(5,1),xhat(6,1),xhat(7,1),xhat(8,1),xhat(9,1),xhat(10,1),xhat(11,1),xhat(12,1),xhat(13,1),xhat(14,1),xhat(15,1),xhat(16,1),xhat(17,1),xhat(18,1),Time.time_step);
    P = F*P*transpose(F) + Q;

    %storing predicted values
    Storage.States_predict(i,:) = [xhat(1,1),xhat(2,1),xhat(3,1),xhat(4,1),xhat(5,1),xhat(6,1),xhat(7,1),xhat(8,1),xhat(9,1),xhat(10,1),xhat(11,1),xhat(12,1),xhat(13,1),xhat(14,1),xhat(15,1),xhat(16,1),xhat(17,1),xhat(18,1)];
    Storage.P_predict = cat(1,Storage.P_predict,P);
    Storage.F = cat(1,Storage.F,F);

    %get first sample
    if i ==2
         z_gps = [GPS1(i,1),GPS1(i,2),GPS1(i,3),GPS2(i,1),GPS2(i,2),GPS2(i,3),GPS3(i,1),GPS3(i,2),GPS3(i,3)];
          %                x1     x1_0           y1    y1_0            z1  z1_0   dx1   dy1   dz1           x2    x2_0           y2    y2_0    z2           z2_0     dx2   dy2   dz2           x3   x3_0            y3    y3_0           z3     z3_0  dx3   dy3   dz3 
          z = H_GPS(GPS1(i,1)    ,0       ,GPS1(i,2)     ,0    ,GPS1(i,3)    ,0    ,0    ,0    ,0   ,GPS2(i,1)      ,0   ,GPS2(i,2)      ,0    ,GPS2(i,3)     ,0      ,0    ,0    ,0   ,GPS3(i,1)     ,0    ,GPS3(i,2)    ,0      ,GPS3(i,3)    ,0     ,0    ,0    ,0,Time.time_step);
    end 


    
  

    if i > 2
    %sample GPS
    if mod(i,s_rate) == 0

     %Adding in bad data for simulation
    Q1 = 0;
    Q2 = 0;
    Q3 = 0;

%     if i == 100 || i == 200 || i == 50
%         Q1 = 1;
%         GPS1(i,:) = [4,5,18]; 
%     end
% 
%     if i == 150 || i == 205 || i == 55
%         Q2 = 1;
%         GPS2(i,:) = [2,10,13]; 
%     end
% 
%     if i == 70 || i == 30 || i == 10
%         Q3 = 1;
%         GPS3(i,:) = [12,0,3]; 
%     end
    

    z_gps = [GPS1(i,1),GPS1(i,2),GPS1(i,3),GPS2(i,1),GPS2(i,2),GPS2(i,3),GPS3(i,1),GPS3(i,2),GPS3(i,3)];
    %                x1     x1_0                  y1          y1_0            z1             z1_0    dx1   dy1   dz1           x2              x2_0           y2          y2_0             z2             z2_0     dx2   dy2   dz2           x3             x3_0            y3            y3_0               z3           z3_0       dx3   dy3   dz3 
    z = H_GPS(GPS1(i,1)    ,Storage.States(i-1,1)   ,GPS1(i,2)     ,Storage.States(i-1,2)   ,GPS1(i,3)    ,Storage.States(i-1,3)    ,0    ,0    ,0   ,GPS2(i,1)      ,Storage.States(i-1,7)   ,GPS2(i,2)     ,Storage.States(i-1,8)    ,GPS2(i,3)     ,Storage.States(i-1,9)     ,0    ,0    ,0   ,GPS3(i,1)     ,Storage.States(i-1,13)     ,GPS3(i,2)    ,Storage.States(i-1,14)      ,GPS3(i,3)    ,Storage.States(i-1,15)     ,0    ,0    ,0,Time.time_step);
    end
    end




    %Find kalman gain
    H = [1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1];
    H = diag(H);
    R =  [0.0172^2;   0.0211^2;  0.0393^2;   (sqrt(2)*0.0172/(Time.time_step*s_rate))^2;   (sqrt(2)*0.0211/(Time.time_step*s_rate))^2;  (sqrt(2)*0.0393/(Time.time_step*s_rate))^2;...
          0.0172^2;   0.0211^2;  0.0393^2;   (sqrt(2)*0.0172/(Time.time_step*s_rate))^2;   (sqrt(2)*0.0211/(Time.time_step*s_rate))^2;  (sqrt(2)*0.0393/(Time.time_step*s_rate))^2;...
          0.0172^2;   0.0211^2;  0.0393^2;   (sqrt(2)*0.0172/(Time.time_step*s_rate))^2;   (sqrt(2)*0.0211/(Time.time_step*s_rate))^2;  (sqrt(2)*0.0393/(Time.time_step*s_rate))^2];
    R = diag(R);
    K = (P*transpose(H))/(H*P*transpose(H) + R);



%     %replace xhat verlocities with measured ones as there is no verlocity prediction
%     xhat(4,1) = z(4,1);xhat(5,1) = z(5,1);xhat(6,1) = z(6,1);
%     xhat(10,1) = z(10,1);xhat(11,1) = z(11,1);xhat(12,1) = z(12,1);
%     xhat(16,1) = z(16,1);xhat(17,1) = z(17,1);xhat(18,1) = z(18,1);


    
    
    %Update
    residual = z - xhat;

    xhat_temp = xhat;
    xhat = xhat + K*(residual);

    if Q1 == 1
        xhat(1:6,1) = xhat_temp(1:6,1);
    end

    if Q2 == 1
        xhat(7:12,1) = xhat_temp(7:12,1);
    end

    if Q3 == 1
        xhat(12:18,1) = xhat_temp(12:18,1);
    end

    P = (eye(18)-K)*P;



    %storing values
    Storage.States(i,:) = [xhat(1,1),xhat(2,1),xhat(3,1),xhat(4,1),xhat(5,1),xhat(6,1),xhat(7,1),xhat(8,1),xhat(9,1),xhat(10,1),xhat(11,1),xhat(12,1),xhat(13,1),xhat(14,1),xhat(15,1),xhat(16,1),xhat(17,1),xhat(18,1)];
    Storage.P = cat(1,Storage.P,P);
    Storage.z(i,:) = transpose(z);
    Storage.residual(i,:) = transpose(residual);
    Storage.z_sampled(i,:) = z_gps;
    %Storage.K(i,:) = transpose(K);
end

Storage.Q = Q;
%%
tstart = 30;
plot3(GPS1(tstart:end,1),GPS1(tstart:end,2),GPS1(tstart:end,3));
hold on
grid on
plot3(GPS2(tstart:end,1),GPS2(tstart:end,2),GPS2(tstart:end,3));

plot3(GPS3(tstart:end,1),GPS3(tstart:end,2),GPS3(tstart:end,3));

hold off
%%
tend = 300;

plot3(Storage.States(tstart:tend,1),Storage.States(tstart:tend,2),Storage.States(tstart:tend,3));
hold on
grid on


plot3(Storage.States(tstart:tend,7),Storage.States(tstart:tend,8),Storage.States(tstart:tend,9));
plot3(Storage.States(tstart:tend,13),Storage.States(tstart:tend,14),Storage.States(tstart:tend,15));

plot3(hip(tstart:tend,1),hip(tstart:tend,2),hip(tstart:tend,3),'g');
plot3(knee(tstart:tend,1),knee(tstart:tend,2),knee(tstart:tend,3),'g');
plot3(ankle(tstart:tend,1),ankle(tstart:tend,2),ankle(tstart:tend,3),'g');
hold off

xlim("auto")
ylim("auto")
zlim("auto")
view([-23.837 58.969])

smooth = 10;
%s1
smoothx1 = smoothdata(Storage.States(:,1),'sgolay',smooth);
smoothy1 = smoothdata(Storage.States(:,2),'sgolay',smooth);
smoothz1 = smoothdata(Storage.States(:,3),'sgolay',smooth);
plot3(smoothx1(tstart:tend,1),smoothy1(tstart:tend,1),smoothz1(tstart:tend,1));
hold on
grid on
%s2
smoothx2 = smoothdata(Storage.States(:,7),'sgolay',smooth);
smoothy2 = smoothdata(Storage.States(:,8),'sgolay',smooth);
smoothz2 = smoothdata(Storage.States(:,9),'sgolay',smooth);
plot3(smoothx2(tstart:tend,1),smoothy2(tstart:tend,1),smoothz2(tstart:tend,1));
%s3
smoothx3 = smoothdata(Storage.States(:,13),'sgolay',smooth);
smoothy3 = smoothdata(Storage.States(:,14),'sgolay',smooth);
smoothz3 = smoothdata(Storage.States(:,15),'sgolay',smooth);
plot3(smoothx3(tstart:tend,1),smoothy3(tstart:tend,1),smoothz3(tstart:tend,1));


plot3(hip(tstart:tend,1),hip(tstart:tend,2),hip(tstart:tend,3),'g');
plot3(knee(tstart:tend,1),knee(tstart:tend,2),knee(tstart:tend,3),'g');
plot3(ankle(tstart:tend,1),ankle(tstart:tend,2),ankle(tstart:tend,3),'g');
hold off
xlim("auto")
ylim("auto")
zlim("auto")
view([-23.837 58.969])

%%
tend = 325;
plot3(Storage.z_sampled(tstart:tend,1),Storage.z_sampled(tstart:tend,2),Storage.z_sampled(tstart:tend,3));
hold on
grid on
plot3(Storage.z_sampled(tstart:tend,4),Storage.z_sampled(tstart:tend,5),Storage.z_sampled(tstart:tend,6));
plot3(Storage.z_sampled(tstart:tend,7),Storage.z_sampled(tstart:tend,8),Storage.z_sampled(tstart:tend,9));
plot3(hip(tstart:tend,1),hip(tstart:tend,2),hip(tstart:tend,3),'g');
plot3(knee(tstart:tend,1),knee(tstart:tend,2),knee(tstart:tend,3),'g');
plot3(ankle(tstart:tend,1),ankle(tstart:tend,2),ankle(tstart:tend,3),'g');
hold off
xlim("auto")
ylim("auto")
zlim("auto")
view([-23.837 58.969])

%%
colnames = {'x1','y1','z1','dx1','dy1','dz1','x2','y2','z2','dx2','dy2','dz2','x3','y3','z3','dx3','dy3','dz3'};

Table_States = array2table(Storage.States,'VariableNames',colnames);
Table_z = array2table(Storage.z,'VariableNames',colnames);
Table_predict = array2table(Storage.States_predict,'VariableNames',colnames);
Table_residual = array2table(Storage.residual,'VariableNames',colnames);

%% RMS

%sampled data
(rms(hip(tstart:tend,1)-Storage.z_sampled(tstart:tend,1))+ rms(hip(tstart:tend,2)-Storage.z_sampled(tstart:tend,2))+ rms(hip(tstart:tend,3)-Storage.z_sampled(tstart:tend,3)))/3
(rms(knee(tstart:tend,1)-Storage.z_sampled(tstart:tend,4))+rms(knee(tstart:tend,2)-Storage.z_sampled(tstart:tend,5))+rms(knee(tstart:tend,3)-Storage.z_sampled(tstart:tend,6)))/3
(rms(ankle(tstart:tend,1)-Storage.z_sampled(tstart:tend,7))+rms(ankle(tstart:tend,2)-Storage.z_sampled(tstart:tend,8))+rms(ankle(tstart:tend,3)-Storage.z_sampled(tstart:tend,9)))/3

%filtered
(rms(hip(tstart:tend,1)-Storage.States(tstart:tend,1))+ rms(hip(tstart:tend,2)-Storage.States(tstart:tend,2))+ rms(hip(tstart:tend,3)-Storage.States(tstart:tend,3)))/3
(rms(knee(tstart:tend,1)-Storage.States(tstart:tend,7))+rms(knee(tstart:tend,2)-Storage.States(tstart:tend,8))+rms(knee(tstart:tend,3)-Storage.States(tstart:tend,9)))/3
(rms(ankle(tstart:tend,1)-Storage.States(tstart:tend,13))+rms(ankle(tstart:tend,2)-Storage.States(tstart:tend,14))+rms(ankle(tstart:tend,3)-Storage.States(tstart:tend,15)))/3

%smoothed
(rms(hip(tstart:tend,1)-smoothx1(tstart:tend,1))+ rms(hip(tstart:tend,2)-smoothy1(tstart:tend,1))+ rms(hip(tstart:tend,3)-smoothz1(tstart:tend,1)))/3
(rms(knee(tstart:tend,1)-smoothx2(tstart:tend,1))+rms(knee(tstart:tend,2)-smoothy2(tstart:tend,1))+rms(knee(tstart:tend,3)-smoothz2(tstart:tend,1)))/3
(rms(ankle(tstart:tend,1)-smoothx3(tstart:tend,1))+rms(ankle(tstart:tend,2)-smoothy3(tstart:tend,1))+rms(ankle(tstart:tend,3)-smoothz3(tstart:tend,1)))/3