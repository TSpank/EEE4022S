clear;
time_step = 0.01;
g = 9.8;


%% Knee Joint

L = 0.47;
t_end = 0.76;
time_vector_1 = 0:time_step:t_end; 
time_vector2_1 = t_end:time_step:10;

th_0_1 = [-pi/4;0];
[time_vector,solution]=ode45(@Sim_pendulum,time_vector_1,th_0_1);
 
for i = 1:length(time_vector)
     orientation(i) = quaternion([0,solution(i,1),0],'euler','XYZ','frame');
end

solution2_1 = zeros(length(time_vector2_1),2);
for i = 1:length(time_vector2_1)
     orientation2_1(i) = quaternion([0,(pi)/6,0],'euler','XYZ','frame');
     solution2_1(i,:) = [(pi)/6,0];
end
orientation = cat(1,transpose(orientation),transpose(orientation2_1));
time_vector = cat(1,time_vector,transpose(time_vector2_1));
solution = cat(1,solution,solution2_1);

%theta
th1 = solution(:,1);
disp("th plot")
plot(time_vector,solution(:,1)*180/pi,'r');

x_pos_2 = zeros(length(time_vector),1);
z_pos_2 = zeros(length(time_vector),1);
y_pos_2 = zeros(length(time_vector),1);

for i = 1:length(time_vector)

    x_pos_2(i) = L*sin(th1(i)) + 0.0172^2*randn;
    z_pos_2(i) = -L*(cos(th1(i)))+0.0393^2*randn;
    y_pos_2(i) = 0 + 0.0211^2*randn;
    
end

%% Ankle joint

L = 0.41;
t_end2 = 0.76;
time_vector_2 = 0:time_step:t_end2; 
time_vector2_2 = t_end2:time_step:10;

th_0_2 = [-pi/2;0];
[time_vector_2,solution_2]=ode45(@Sim_pendulum,time_vector_1,th_0_2);
 
for i = 1:length(time_vector_2)
     orientation_2(i) = quaternion([0,solution_2(i,1),0],'euler','XYZ','frame');
end

ddd =1.1
solution2_2 = zeros(length(time_vector2_2),2);
for i = 1:length(time_vector2_2)
     orientation2_2(i) = quaternion([0,(pi+ddd)/6,0],'euler','XYZ','frame');
     solution2_2(i,:) = [(pi+ddd)/6,0];
end
orientation_2 = cat(1,transpose(orientation_2),transpose(orientation2_2));
time_vector_2 = cat(1,time_vector_2,transpose(time_vector2_2));
solution_2 = cat(1,solution_2,solution2_2);


%theta
th2 = solution_2(:,1);
disp("th plot")
plot(time_vector,solution_2(:,1)*180/pi,'r');

x_pos_1 = zeros(length(time_vector),1);
z_pos_1 = zeros(length(time_vector),1);
y_pos_1 = zeros(length(time_vector),1);

for i = 1:length(time_vector)

    x_pos_1(i) = L*sin(th2(i))+ 0.0172^2*randn;
    z_pos_1(i) = -L*(cos(th2(i))) + 0.0393^2*randn;
    y_pos_1(i) = 0 + 0.0211^2*randn;
    
end


%% Hip joint

L = 0.19;

t_end3 = 0.76;
time_vector_3 = 0:time_step:t_end3; 
time_vector2_3 = t_end3:time_step:10;

th_0_3 = [-pi/6;0];
[time_vector_3,solution_3]=ode45(@Sim_pendulum,time_vector_3,th_0_3);
 
for i = 1:length(time_vector_3)
     orientation_3(i) = quaternion([0,solution_3(i,1),0],'euler','XYZ','frame');
end

ddd =-0.97;
solution2_3 = zeros(length(time_vector2_3),2);
for i = 1:length(time_vector2_3)
     orientation2_3(i) = quaternion([0,(pi+ddd)/6,0],'euler','XYZ','frame');
     solution2_3(i,:) = [(pi+ddd)/6,0];
end

orientation_3 = cat(1,transpose(orientation_3),transpose(orientation2_3));
time_vector_3 = cat(1,time_vector_3,transpose(time_vector2_3));
solution_3 = cat(1,solution_3,solution2_3);


%theta
th3 = solution_3(:,1);
disp("th plot")
plot(time_vector,solution_3(:,1)*180/pi,'r');

x_pos_3 = zeros(length(time_vector),1);
z_pos_3 = zeros(length(time_vector),1);
y_pos_3 = zeros(length(time_vector),1);

for i = 1:length(time_vector)

    x_pos_3(i) = L*sin(th3(i))+ 0.0172^2*randn;
    z_pos_3(i) = 0 + 0.0393^2*randn;
    y_pos_3(i) = L*(cos(th3(i)))+ 0.0211^2*randn;
    
end

hip = [x_pos_3(:,1),y_pos_3(:,1),z_pos_3(:,1)];
%% get knee and ankle to ratate with the hip

knee = zeros(length(x_pos_1(:,1)),3);
ankle = zeros(length(x_pos_1(:,1)),3);

for i = 1:length(time_vector)
    R = rotationMatrix(0,0,-th3(i));
    knee(i,:) = hip(i,:)+transpose(R*[x_pos_2(i,1)             ;y_pos_2(i,1)             ;z_pos_2(i,1)]);
    ankle(i,:) = hip(i,:)+transpose(R*[x_pos_2(i,1)+x_pos_1(i,1);y_pos_2(i,1)+y_pos_1(i,1);z_pos_2(i,1)+z_pos_1(i,1)]);
end
%%

plot3(ankle(:,1),ankle(:,2),ankle(:,3));
hold on
grid on
plot3(knee(:,1),knee(:,2),knee(:,3));
plot3(hip(:,1),hip(:,2),hip(:,3));