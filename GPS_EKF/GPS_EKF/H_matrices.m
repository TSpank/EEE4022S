%% Measurement

syms x1 y1 z1 dx1 dy1 dz1...
     x1_0 y1_0 z1_0...
     x2 y2 z2 dx2 dy2 dz2...
     x2_0 y2_0 z2_0...
     x3 y3 z3 dx3 dy3 dz3...
     x3_0 y3_0 z3_0...

syms L dt;

vars_GPS = [x1 x1_0 y1 y1_0 z1 z1_0 dx1 dy1 dz1...
            x2 x2_0 y2 y2_0 z2 z2_0 dx2 dy2 dz2...
            x3 x3_0 y3 y3_0 z3 z3_0 dx3 dy3 dz3 dt];
states_GPS = [x1; y1; z1; dx1; dy1; dz1; x2; y2; z2; dx2; dy2; dz2; x3; y3; z3; dx3; dy3; dz3];

%get accelration into innertial frame 
%sensor 1
    % calculate verlocities
dx1 = (x1 - x1_0)/dt;
dy1 = (y1 - y1_0)/dt;
dz1 = (z1 - z1_0)/dt;

%sensor 2
    % calculate verlocities
dx2 = (x2 - x2_0)/dt;
dy2 = (y2 - y2_0)/dt;
dz2 = (z2 - z2_0)/dt; 

%sensor 3
    % calculate verlocities
dx3 = (x3 - x3_0)/dt;
dy3 = (y3 - y3_0)/dt;
dz3 = (z3 - z3_0)/dt;

H_GPS_out = [x1; y1; z1; dx1; dy1; dz1; x2; y2; z2; dx2; dy2; dz2; x3; y3; z3; dx3; dy3; dz3];

matlabFunction(H_GPS_out, 'file','C:\Users\user\Documents\MATLAB\Pendulum\GPS sim\H_GPS','Vars', vars_GPS);