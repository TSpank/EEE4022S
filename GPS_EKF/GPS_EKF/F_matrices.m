syms x1 y1 z1 dx1 dy1 dz1...
     x2 y2 z2 dx2 dy2 dz2...
     x3 y3 z3 dx3 dy3 dz3...

syms L dt;

vars_GPS = [x1 y1 z1 dx1 dy1 dz1 x2 y2 z2 dx2 dy2 dz2 x3 y3 z3 dx3 dy3 dz3 dt];
states_GPS = [x1; y1; z1; dx1; dy1; dz1; x2; y2; z2; dx2; dy2; dz2; x3; y3; z3; dx3; dy3; dz3];

%get accelration into innertial frame 
%sensor 1
    % update positions s1
x1 = x1 + dx1*dt 
y1 = y1 + dy1*dt
z1 = z1 + dz1*dt 

%sensor 2
    % update positions s1
x2 = x2 + dx2*dt 
y2 = y2 + dy2*dt
z2 = z2 + dz2*dt 

%sensor 3
    % update positions s1
x3 = x3 + dx3*dt 
y3 = y3 + dy3*dt
z3 = z3 + dz3*dt 

F_GPS_out = [x1; y1; z1; dx1; dy1; dz1; x2; y2; z2; dx2; dy2; dz2; x3; y3; z3; dx3; dy3; dz3];
dF_F_GPS_out = jacobian(F_GPS_out, states_GPS)

matlabFunction(F_GPS_out, 'file','C:\Users\user\Documents\MATLAB\Pendulum\GPS sim\F_GPS','Vars', vars_GPS);
matlabFunction(dF_F_GPS_out, 'file','C:\Users\user\Documents\MATLAB\Pendulum\GPS sim\dF_GPS','Vars', vars_GPS);