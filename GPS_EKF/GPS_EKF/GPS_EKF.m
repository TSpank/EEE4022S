%time
Time.time_step = 0.1;
Time.t_end = 10;
Time.time_vector = 0:Time.time_step:Time.t_end;
len = length(Time.time_vector)+1;

%for plotting
tstart = 30;
tend = 300;

%get data
    %gps
GPS1 = S_hip;
GPS2 = S_knee;
GPS3 = S_ankle;

%            x1         y1       z1    dx1   dy1   dz1           x2        y2        z2   dx2   dy2   dz2           x3        y3       z3    dx3   dy3   dz3
x_0 = [GPS1(1,2);GPS1(1,3);GPS1(1,4)    ;0    ;0    ;0   ;GPS2(1,2);GPS2(1,3);GPS2(1,4)    ;0    ;0    ;0   ;GPS3(1,2);GPS3(1,3);GPS3(1,4)    ;0    ;0    ;0];
P_0 = [0.013^2;   0.014^2;  0.009^2;   0;   0;  0;...
       0.013^2;   0.014^2;  0.009^2;   0;   0;  0;...
       0.013^2;   0.014^2;  0.009^2;   0;   0;  0];

P_0 = diag(P_0);

%%
%storage
Storage.States = zeros(length(Time.time_vector),18);
Storage.States_predict = zeros(length(Time.time_vector),18);
Storage.K = zeros(length(Time.time_vector),18);

Storage.P = diag([0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0]);
Storage.P_predict = diag([0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0]);

Storage.F = diag([0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0]);
Storage.z = zeros(length(Time.time_vector),18);
Storage.z_sampled = zeros(length(Time.time_vector),9);
Storage.residual = zeros(length(Time.time_vector),18);
Storage.Q = eye(18);

Storage.States(1,:) = x_0;
%initialize filter
xhat = x_0;
P = P_0;
residual = [0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0];


    %set the sampling rate
    s_rate = 10;


%%KALMAN FILTER
for i = 2:length(GPS1(:,1))
    %Process noise                                                                                                                                                                         
%                   x1         y1          z1                             dx1                                dy1                             dz1        
    noise = [(sqrt(2)*0.013/(Time.time_step*s_rate))^2*Time.time_step*s_rate;   (sqrt(2)*0.014/(Time.time_step*s_rate))^2*Time.time_step*s_rate;  (sqrt(2)*0.009/(Time.time_step*s_rate))^2*Time.time_step*s_rate;   (sqrt(2)*0.013/(Time.time_step*s_rate))^2;   (sqrt(2)*0.014/(Time.time_step*s_rate))^2;  (sqrt(2)*0.009/(Time.time_step*s_rate))^2;...
             (sqrt(2)*0.013/(Time.time_step*s_rate))^2*Time.time_step*s_rate;   (sqrt(2)*0.014/(Time.time_step*s_rate))^2*Time.time_step*s_rate;  (sqrt(2)*0.009/(Time.time_step*s_rate))^2*Time.time_step*s_rate;   (sqrt(2)*0.013/(Time.time_step*s_rate))^2;   (sqrt(2)*0.014/(Time.time_step*s_rate))^2;  (sqrt(2)*0.009/(Time.time_step*s_rate))^2;...
             (sqrt(2)*0.013/(Time.time_step*s_rate))^2*Time.time_step*s_rate;   (sqrt(2)*0.014/(Time.time_step*s_rate))^2*Time.time_step*s_rate;  (sqrt(2)*0.009/(Time.time_step*s_rate))^2*Time.time_step*s_rate;   (sqrt(2)*0.013/(Time.time_step*s_rate))^2;   (sqrt(2)*0.014/(Time.time_step*s_rate))^2;  (sqrt(2)*0.009/(Time.time_step*s_rate))^2];
    Q = diag(noise);

    %sensor 1
    %Predict
    %                   x1       y1        z1       dx1       dy1       dz1        x2        y2        z2        dx2        dy2        dz2         x3         y3         z3        dx3        dy3        dz3
    xhat = F_GPS(xhat(1,1),xhat(2,1),xhat(3,1),xhat(4,1),xhat(5,1),xhat(6,1),xhat(7,1),xhat(8,1),xhat(9,1),xhat(10,1),xhat(11,1),xhat(12,1),xhat(13,1),xhat(14,1),xhat(15,1),xhat(16,1),xhat(17,1),xhat(18,1),Time.time_step);
    F =   dF_GPS(xhat(1,1),xhat(2,1),xhat(3,1),xhat(4,1),xhat(5,1),xhat(6,1),xhat(7,1),xhat(8,1),xhat(9,1),xhat(10,1),xhat(11,1),xhat(12,1),xhat(13,1),xhat(14,1),xhat(15,1),xhat(16,1),xhat(17,1),xhat(18,1),Time.time_step);
    P = F*P*transpose(F) + Q;

    %storing predicted values
    Storage.States_predict(i,:) = [xhat(1,1),xhat(2,1),xhat(3,1),xhat(4,1),xhat(5,1),xhat(6,1),xhat(7,1),xhat(8,1),xhat(9,1),xhat(10,1),xhat(11,1),xhat(12,1),xhat(13,1),xhat(14,1),xhat(15,1),xhat(16,1),xhat(17,1),xhat(18,1)];
    Storage.P_predict = cat(1,Storage.P_predict,P);
    Storage.F = cat(1,Storage.F,F);

    %get first sample
    if i ==2
          %                x1     x1_0           y1    y1_0            z1  z1_0   dx1   dy1   dz1           x2    x2_0           y2    y2_0    z2           z2_0     dx2   dy2   dz2           x3   x3_0            y3    y3_0           z3     z3_0  dx3   dy3   dz3 
          z = H_GPS(GPS1(i,2)    ,0       ,GPS1(i,3)     ,0    ,GPS1(i,4)    ,0    ,0    ,0    ,0   ,GPS2(i,2)      ,0   ,GPS2(i,3)      ,0    ,GPS2(i,4)     ,0      ,0    ,0    ,0   ,GPS3(i,2)     ,0    ,GPS3(i,3)    ,0      ,GPS3(i,4)    ,0     ,0    ,0    ,0,Time.time_step);
    end 


    
    
  

    if i > 2
    %sample GPS
    %                x1     x1_0                     y1             y1_0                      z1                           z1_0    dx1   dy1   dz1           x2                        x2_0          y2                     y2_0              z2                       z2_0     dx2   dy2   dz2           x3                       x3_0              y3                     y3_0               z3                      z3_0       dx3   dy3   dz3 
    z = H_GPS(GPS1(i,2)    ,Storage.States(i-1,1)   ,GPS1(i,3)     ,Storage.States(i-1,2)   ,GPS1(i,4)    ,Storage.States(i-1,3)    ,0    ,0    ,0   ,GPS2(i,2)      ,Storage.States(i-1,7)   ,GPS2(i,3)     ,Storage.States(i-1,8)    ,GPS2(i,4)     ,Storage.States(i-1,9)     ,0    ,0    ,0   ,GPS3(i,2)     ,Storage.States(i-1,13)     ,GPS3(i,3)    ,Storage.States(i-1,14)      ,GPS3(i,4)    ,Storage.States(i-1,15)     ,0    ,0    ,0,Time.time_step);
    end




    %Find kalman gain
    H = [1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1];
    H = diag(H);
    R =  [0.013^2;   0.014^2;  0.009^2;   (sqrt(2)*0.013/(Time.time_step*s_rate))^2;   (sqrt(2)*0.014/(Time.time_step*s_rate))^2;  (sqrt(2)*0.009/(Time.time_step*s_rate))^2;...
          0.013^2;   0.014^2;  0.009^2;   (sqrt(2)*0.013/(Time.time_step*s_rate))^2;   (sqrt(2)*0.014/(Time.time_step*s_rate))^2;  (sqrt(2)*0.009/(Time.time_step*s_rate))^2;...
          0.013^2;   0.009^2;  0.009^2;   (sqrt(2)*0.013/(Time.time_step*s_rate))^2;   (sqrt(2)*0.014/(Time.time_step*s_rate))^2;  (sqrt(2)*0.009/(Time.time_step*s_rate))^2];
    R = diag(R);
    
    xy = 0.002;
    xz = 0.014;
    yz = 0.009;

    dxy = (sqrt(2)*0.002/(Time.time_step*s_rate))^2
    dxz = (sqrt(2)*0.014/(Time.time_step*s_rate))^2
    dyz = (sqrt(2)*0.009/(Time.time_step*s_rate))^2

    %s1
    R(1,2) = xy;       R(2,1) = xy;      R(3,2) = yz;        
    R(1,3) = xz;       R(3,1) = xz;      R(2,3) = yz; 
    R(1+3,2+3) = dxy;       R(2+3,1+3) = dxy;      R(3+3,2+3) = dyz;        
    R(1+3,3+3) = dxz;       R(3+3,1+3) = dxz;      R(2+3,3+3) = dyz; 

    %s2
    R(1+6,2+6) = xy;       R(2+6,1+6) = xy;      R(3+6,2+6) = yz;        
    R(1+6,3+6) = xz;       R(3+6,1+6) = xz;      R(2+6,3+6) = yz;   
    R(1+9,2+9) = dxy;       R(2+9,1+9) = dxy;      R(3+9,2+9) = dyz;        
    R(1+9,3+9) = dxz;       R(3+9,1+9) = dxz;      R(2+9,3+9) = dyz;

    %s3
    R(1+12,2+12) = xy;       R(2+12,1+12) = xy;      R(3+12,2+12) = yz;        
    R(1+12,3+12) = xz;       R(3+12,1+12) = xz;      R(2+12,3+12) = yz;
    R(1+15,2+15) = dxy;       R(2+15,1+15) = dxy;      R(3+15,2+15) = dyz;        
    R(1+15,3+15) = dxz;       R(3+15,1+15) = dxz;      R(2+15,3+15) = dyz;
     



    K = (P*transpose(H))/(H*P*transpose(H) + R);



%     %replace xhat verlocities with measured ones as there is no verlocity prediction
%     xhat(4,1) = z(4,1);xhat(5,1) = z(5,1);xhat(6,1) = z(6,1);
%     xhat(10,1) = z(10,1);xhat(11,1) = z(11,1);xhat(12,1) = z(12,1);
%     xhat(16,1) = z(16,1);xhat(17,1) = z(17,1);xhat(18,1) = z(18,1);


    
    
    %Update
    residual = z - xhat;
    residual_sat = residual;
    xhat_temp = xhat;
    xhat = xhat + K*(residual);



    fac = 3;
    if GPS1(i,5) == 2 && ((abs(residual(1)) > 0.0172*fac) || (abs(residual(2)) > 0.0172*fac) || (abs(residual(3)) > 0.0172*fac))
        if abs(residual(1)) > 0.0172*fac
            residual_sat(1,1) = (-residual(1)/-residual(1))*0.0172*fac;
        end

        if abs(residual(2)) > 0.0172*fac
            residual_sat(2,1) = (-residual(2)/-residual(2))*0.0172*fac;
        end

        if abs(residual(3)) > 0.0172*fac
            residual_sat(3,1) = (-residual(3)/-residual(3))*0.0172*fac;
        end

        xhat = xhat_temp + K*(residual_sat);
    end

    if GPS1(i,5) == 2 && ((abs(residual(7)) > 0.0172*fac) || (abs(residual(8)) > 0.0172*fac) || (abs(residual(9)) > 0.0172*fac))
        if abs(residual(7)) > 0.0172*fac
            residual_sat(7) = (-residual(7)/-residual(7))*0.0172*fac;
        end

        if abs(residual(8)) > 0.0172*fac
            residual_sat(8) = (-residual(8)/-residual(8))*0.0172*fac;
        end

        if abs(residual(9)) > 0.0172*fac
            residual_sat(9) = (-residual(9)/-residual(9))*0.0172*fac;
        end

        xhat = xhat_temp + K*(residual_sat);
    end

    if GPS1(i,5) == 2 && ((abs(residual(13)) > 0.0172*fac) || (abs(residual(14)) > 0.0172*fac) || (abs(residual(15)) > 0.0172*fac))
        if abs(residual(13)) > 0.0172*fac
            residual_sat(13) = (-residual(13)/-residual(13))*0.0172*fac;
        end

        if abs(residual(14)) > 0.0172*fac
            residual_sat(14) = (-residual(14)/-residual(14))*0.0172*fac;
        end

        if abs(residual(15)) > 0.0172*fac
            residual_sat(15) = (-residual(15)/-residual(15))*0.0172*fac;
        end

        xhat = xhat_temp + K*(residual_sat);
    end


    if GPS1(i,5) ~= 2
        xhat(1:6,1) = xhat_temp(1:6,1);
    end

    if GPS2(i,5) ~= 2
        xhat(7:12,1) = xhat_temp(7:12,1);
    end

    if GPS3(i,5) ~= 2
        xhat(12:18,1) = xhat_temp(12:18,1);
    end

    P = (eye(18)-K)*P;



    %storing values
    Storage.States(i,:) = [xhat(1,1),xhat(2,1),xhat(3,1),xhat(4,1),xhat(5,1),xhat(6,1),xhat(7,1),xhat(8,1),xhat(9,1),xhat(10,1),xhat(11,1),xhat(12,1),xhat(13,1),xhat(14,1),xhat(15,1),xhat(16,1),xhat(17,1),xhat(18,1)];
    Storage.P = cat(1,Storage.P,P);
    Storage.z(i,:) = transpose(z);
    Storage.residual(i,:) = transpose(residual);
    %Storage.K(i,:) = transpose(K);
end

Storage.Q = Q;
%%
tstart = 150;

plot3(GPS1(tstart:end,2),GPS1(tstart:end,3),GPS1(tstart:end,4));
hold on
grid on
plot3(GPS2(tstart:end,2),GPS2(tstart:end,3),GPS2(tstart:end,4));

plot3(GPS3(tstart:end,2),GPS3(tstart:end,3),GPS3(tstart:end,4));

hold off
%%
tend = 250;

plot(Storage.States(tstart:tend,1))
plot(Storage.States(tstart:tend,2))
hold on 
plot(Storage.residual(tstart:tend,3))
hold off
plot(Storage.States(tstart:tend,3))
scatter3(Storage.States(tstart:tend,1),Storage.States(tstart:tend,2),Storage.States(tstart:tend,3),'r');
hold on
grid on


scatter3(Storage.States(tstart:tend,7),Storage.States(tstart:tend,8),Storage.States(tstart:tend,9),'g');
scatter3(Storage.States(tstart:tend,13),Storage.States(tstart:tend,14),Storage.States(tstart:tend,15),'b');

hold off


smooth = 10;
%s1
smoothx1 = smoothdata(Storage.States(:,1),'sgolay',smooth);
smoothy1 = smoothdata(Storage.States(:,2),'sgolay',smooth);
smoothz1 = smoothdata(Storage.States(:,3),'sgolay',smooth);
plot3(smoothx1(tstart:tend,1),smoothy1(tstart:tend,1),smoothz1(tstart:tend,1));
hold on
grid on
%s2
smoothx2 = smoothdata(Storage.States(:,7),'sgolay',smooth);
smoothy2 = smoothdata(Storage.States(:,8),'sgolay',smooth);
smoothz2 = smoothdata(Storage.States(:,9),'sgolay',smooth);
plot3(smoothx2(tstart:tend,1),smoothy2(tstart:tend,1),smoothz2(tstart:tend,1));
%s3
smoothx3 = smoothdata(Storage.States(:,13),'sgolay',smooth);
smoothy3 = smoothdata(Storage.States(:,14),'sgolay',smooth);
smoothz3 = smoothdata(Storage.States(:,15),'sgolay',smooth);

scatter3(smoothx1(tstart:tend,1),smoothy1(tstart:tend,1),smoothz1(tstart:tend,1),'r');
hold on
grid on
scatter3(smoothx2(tstart:tend,1),smoothy2(tstart:tend,1),smoothz2(tstart:tend,1),'g');
scatter3(smoothx3(tstart:tend,1),smoothy3(tstart:tend,1),smoothz3(tstart:tend,1),'b');
hold off





%%
colnames = {'x1','y1','z1','dx1','dy1','dz1','x2','y2','z2','dx2','dy2','dz2','x3','y3','z3','dx3','dy3','dz3'};

Table_States = array2table(Storage.States,'VariableNames',colnames);
Table_z = array2table(Storage.z,'VariableNames',colnames);
Table_predict = array2table(Storage.States_predict,'VariableNames',colnames);
Table_residual = array2table(Storage.residual,'VariableNames',colnames);

%%