%% side test

date = '2022/11/01';

start = '';
fid = fopen('C:\Users\user\Documents\leg\lying on side\t1\sens3GNSS.csv');
s = fscanf(fid, '%c', [1 inf]);  % read all text from file
fclose(fid);
% Replace string
s = strrep(s, ':', '');
s = strrep(s, date, '');
% Save data
fid = fopen('C:\Users\user\Documents\leg\lying on side\t1\sens3GNSS_new.csv','w');
s = fprintf(fid, '%c', s);  % read all text from file
fclose(fid);

file ='C:\Users\user\Documents\leg\lying on side\t1\sens3GNSS_new.csv';
S_ankle = readmatrix(file);

start = '';
fid = fopen('C:\Users\user\Documents\leg\lying on side\t1\sens4GNSS.csv');
s = fscanf(fid, '%c', [1 inf]);  % read all text from file
fclose(fid);
% Replace string
s = strrep(s, ':', '');
s = strrep(s, date, '');
% Save data
fid = fopen('C:\Users\user\Documents\leg\lying on side\t1\sens4GNSS_new.csv','w');
s = fprintf(fid, '%c', s);  % read all text from file
fclose(fid);

file ='C:\Users\user\Documents\leg\lying on side\t1\sens4GNSS_new.csv';
S_knee = readmatrix(file);

start = '';
fid = fopen('C:\Users\user\Documents\leg\lying on side\t1\sens5GNSS.csv');
s = fscanf(fid, '%c', [1 inf]);  % read all text from file
fclose(fid);
% Replace string
s = strrep(s, ':', '');
s = strrep(s, date, '');
% Save data
fid = fopen('C:\Users\user\Documents\leg\lying on side\t1\sens5GNSS_new.csv','w');
s = fprintf(fid, '%c', s);  % read all text from file
fclose(fid);

file ='C:\Users\user\Documents\leg\lying on side\t1\sens5GNSS_new.csv';
S_hip = readmatrix(file);

last = 1600;
start_a = 100;
end_a   = 1600;

start = S_ankle(start_a,1)
start_k = find(S_knee(:,1)==start)
start_h = find(S_hip(:,1)==start)

ends = S_ankle(end_a,1)
end_k = find(S_knee(:,1)==ends)
end_h = find(S_hip(:,1)==ends)

S_ankle=S_ankle(start_a:end_a,:);
S_knee  =S_knee(start_k:end_k,:);
S_hip    =S_hip(start_h:end_h,:);

plot3(S_ankle(:,2),S_ankle(:,3),S_ankle(:,4))
grid on
hold on 
plot3(S_knee(:,2),S_knee(:,3),S_knee(:,4))
plot3(S_hip(:,2),S_hip(:,3),S_hip(:,4))
hold off

%% Standing test

date = '2022/11/01';
start = '';
fid = fopen('C:\Users\user\Documents\leg\standing\sens6GNSS.csv');
s = fscanf(fid, '%c', [1 inf]);  % read all text from file
fclose(fid);
% Replace string
s = strrep(s, ':', '');
s = strrep(s, date, '');
% Save data
fid = fopen('C:\Users\user\Documents\leg\standing\sens6GNSS_new.csv','w');
s = fprintf(fid, '%c', s);  % read all text from file
fclose(fid);

file ='C:\Users\user\Documents\leg\standing\sens6GNSS_new.csv';
S_ankle = readmatrix(file);

start = '';
fid = fopen('C:\Users\user\Documents\leg\standing\sens4GNSS.csv');
s = fscanf(fid, '%c', [1 inf]);  % read all text from file
fclose(fid);
% Replace string
s = strrep(s, ':', '');
s = strrep(s, date, '');
% Save data
fid = fopen('C:\Users\user\Documents\leg\standing\sens4GNSS_new.csv','w');
s = fprintf(fid, '%c', s);  % read all text from file
fclose(fid);

file ='C:\Users\user\Documents\leg\standing\sens4GNSS_new.csv';
S_knee = readmatrix(file);

start = '';
fid = fopen('C:\Users\user\Documents\leg\standing\sens5GNSS.csv');
s = fscanf(fid, '%c', [1 inf]);  % read all text from file
fclose(fid);
% Replace string
s = strrep(s, ':', '');
s = strrep(s, date, '');
% Save data
fid = fopen('C:\Users\user\Documents\leg\standing\sens5GNSS_new.csv','w');
s = fprintf(fid, '%c', s);  % read all text from file
fclose(fid);

file ='C:\Users\user\Documents\leg\standing\sens5GNSS_new.csv';
S_hip = readmatrix(file);

start_a = 250;
end_a   = 600;

start = S_ankle(start_a,1)
start_k = find(S_knee(:,1)==start)
start_h = find(S_hip(:,1)==start)

ends = S_ankle(end_a,1)
end_k = find(S_knee(:,1)==ends)
end_h = find(S_hip(:,1)==ends)

S_ankle=S_ankle(start_a:end_a,:);
S_knee  =S_knee(start_k:end_k,:);
S_hip    =S_hip(start_h:end_h,:);

plot3(S_ankle(:,2),S_ankle(:,3),S_ankle(:,4),'r')
grid on
hold on 
plot3(S_knee(:,2),S_knee(:,3),S_knee(:,4),'b')
plot3(S_hip(:,2),S_hip(:,3),S_hip(:,4),'g')
hold off

%% walking

date = '2022/11/01';
start = '';
fid = fopen('C:\Users\user\Documents\leg\slow walking no arms\sens6GNSS.csv');
s = fscanf(fid, '%c', [1 inf]);  % read all text from file
fclose(fid);
% Replace string
s = strrep(s, ':', '');
s = strrep(s, date, '');
% Save data
fid = fopen('C:\Users\user\Documents\leg\slow walking no arms\sens6GNSS_new.csv','w');
s = fprintf(fid, '%c', s);  % read all text from file
fclose(fid);

file ='C:\Users\user\Documents\leg\slow walking no arms\sens6GNSS_new.csv';
S_ankle = readmatrix(file);

start = '';
fid = fopen('C:\Users\user\Documents\leg\slow walking no arms\sens4GNSS.csv');
s = fscanf(fid, '%c', [1 inf]);  % read all text from file
fclose(fid);
% Replace string
s = strrep(s, ':', '');
s = strrep(s, date, '');
% Save data
fid = fopen('C:\Users\user\Documents\leg\slow walking no arms\sens4GNSS_new.csv','w');
s = fprintf(fid, '%c', s);  % read all text from file
fclose(fid);

file ='C:\Users\user\Documents\leg\slow walking no arms\sens4GNSS_new.csv';
S_knee = readmatrix(file);

start = '';
fid = fopen('C:\Users\user\Documents\leg\slow walking no arms\sens5GNSS.csv');
s = fscanf(fid, '%c', [1 inf]);  % read all text from file
fclose(fid);
% Replace string
s = strrep(s, ':', '');
s = strrep(s, date, '');
% Save data
fid = fopen('C:\Users\user\Documents\leg\slow walking no arms\sens5GNSS_new.csv','w');
s = fprintf(fid, '%c', s);  % read all text from file
fclose(fid);

file ='C:\Users\user\Documents\leg\slow walking no arms\sens5GNSS_new.csv';
S_hip = readmatrix(file);

start_a = 250;
end_a   = 600;

start = S_ankle(start_a,1)
start_k = find(S_knee(:,1)==start)
start_h = find(S_hip(:,1)==start)

ends = S_ankle(end_a,1)
end_k = find(S_knee(:,1)==ends)
end_h = find(S_hip(:,1)==ends)

S_ankle=S_ankle(start_a:end_a,:);
S_knee  =S_knee(start_k:end_k,:);
S_hip    =S_hip(start_h:end_h,:);

plot3(S_ankle(:,2),S_ankle(:,3),S_ankle(:,4),'r')
grid on
hold on 
plot3(S_knee(:,2),S_knee(:,3),S_knee(:,4),'b')
plot3(S_hip(:,2),S_hip(:,3),S_hip(:,4),'g')
hold off

%% side test

date = '2022/11/01';

start = '';
fid = fopen('C:\Users\user\Documents\leg\lying on side\t2\sens6GNSS.csv');
s = fscanf(fid, '%c', [1 inf]);  % read all text from file
fclose(fid);
% Replace string
s = strrep(s, ':', '');
s = strrep(s, date, '');
% Save data
fid = fopen('C:\Users\user\Documents\leg\lying on side\t2\sens6GNSS_new.csv','w');
s = fprintf(fid, '%c', s);  % read all text from file
fclose(fid);

file ='C:\Users\user\Documents\leg\lying on side\t2\sens6GNSS_new.csv';
S_ankle = readmatrix(file);

start = '';
fid = fopen('C:\Users\user\Documents\leg\lying on side\t2\sens6GNSS.csv');
s = fscanf(fid, '%c', [1 inf]);  % read all text from file
fclose(fid);
% Replace string
s = strrep(s, ':', '');
s = strrep(s, date, '');
% Save data
fid = fopen('C:\Users\user\Documents\leg\lying on side\t2\sens4GNSS_new.csv','w');
s = fprintf(fid, '%c', s);  % read all text from file
fclose(fid);

file ='C:\Users\user\Documents\leg\lying on side\t2\sens4GNSS_new.csv';
S_knee = readmatrix(file);

start = '';
fid = fopen('C:\Users\user\Documents\leg\lying on side\t2\sens5GNSS.csv');
s = fscanf(fid, '%c', [1 inf]);  % read all text from file
fclose(fid);
% Replace string
s = strrep(s, ':', '');
s = strrep(s, date, '');
% Save data
fid = fopen('C:\Users\user\Documents\leg\lying on side\t2\sens5GNSS_new.csv','w');
s = fprintf(fid, '%c', s);  % read all text from file
fclose(fid);

file ='C:\Users\user\Documents\leg\lying on side\t2\sens5GNSS_new.csv';
S_hip = readmatrix(file);
%%

last = 1600;
start_a = 300;
end_a   = 1600;

start = S_ankle(start_a,1)
start_k = find(S_knee(:,1)==start)
start_h = find(S_hip(:,1)==start)

ends = S_ankle(end_a,1)
end_k = find(S_knee(:,1)==ends)
end_h = find(S_hip(:,1)==ends)

S_ankle=S_ankle(start_a:end_a,:);
S_knee  =S_knee(start_k:end_k,:);
S_hip    =S_hip(start_h:end_h,:);

plot3(S_ankle(:,2),S_ankle(:,3),S_ankle(:,4))
grid on
hold on 
plot3(S_knee(:,2),S_knee(:,3),S_knee(:,4))
plot3(S_hip(:,2),S_hip(:,3),S_hip(:,4))
hold off