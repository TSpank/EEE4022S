%%
close all;
L = 1;
writerObj = VideoWriter('EKF');
writerObj.FrameRate = 30;
open(writerObj);

%% Positions

P_S1 = [Storage.States(:,1),Storage.States(:,2),Storage.States(:,3)];
P_S2 = [Storage.States(:,7),Storage.States(:,8),Storage.States(:,9)];
P_S3 = [Storage.States(:,13),Storage.States(:,14),Storage.States(:,15)];

%% Create Figure Handles
figure()
axis([-15 15 -15 15 -15 15])
%axis([-1 5 -1 6 -5 15])
%axis([-2 2 0 5 -2 2])
grid on;
hold on;

% Rods
r1 = line('Color', 'b', 'LineWidth', 1);
%r2 = line('Color', 'm', 'LineWidth', 1);
r3 = line('Color', 'g', 'LineWidth', 1);

%sensors 
b1 = plot3(P_S1(1,1),P_S1(1,2),P_S1(1,3), 'o','MarkerFaceColor','red','MarkerSize',3);
b2 = plot3(P_S2(1,1),P_S2(1,2),P_S2(1,3), 'o','MarkerFaceColor','red','MarkerSize',3);
b3 = plot3(P_S3(1,1),P_S3(1,2),P_S3(1,3), 'o','MarkerFaceColor','red','MarkerSize',3);

% general plot setup
xlabel({'North (m)'},'FontSize',14,'FontName','AvantGarde');
ylabel({'East (m)'},'FontSize',14,'FontName','AvantGarde');
zlabel({'Up (m)'},'FontSize',14,'FontName','AvantGarde');
    
title({'Leg animation'},'FontWeight','bold','FontSize',20,...
    'FontName','AvantGarde');

view(-37.5,30)

%% Update the rod and masses in the plot in a loop
FigHandle = gcf;

for i = 1:length(Storage.States(:,1))
    %sensor1 
    set(r1, 'XData', [P_S2(i,1), P_S1(i,1)]);
    set(r1, 'YData', [P_S2(i,2), P_S1(i,2)]);
    set(r1, 'ZData', [P_S2(i,3), P_S1(i,3)]);
    set(b1, 'XData', P_S1(i,1));
    set(b1, 'YData', P_S1(i,2));
    set(b1, 'ZData', P_S1(i,3));
    
    %sensor2
%     set(r2, 'XData', [x(i), P_S2(i,1)]);
%     set(r2, 'YData', [y(i), P_S2(i,2)]);
%     set(r2, 'ZData', [z(i), P_S2(i,3)]);
    set(b2, 'XData', P_S2(i,1));
    set(b2, 'YData', P_S2(i,2));
    set(b2, 'ZData', P_S2(i,3));
    
    %sensor3
    set(r3, 'XData', [P_S2(i,1), P_S3(i,1)]);
    set(r3, 'YData', [P_S2(i,2), P_S3(i,2)]);
    set(r3, 'ZData', [P_S2(i,3), P_S3(i,3)]);
    set(b3, 'XData', P_S3(i,1));
    set(b3, 'YData', P_S3(i,2));
    set(b3, 'ZData', P_S3(i,3));
    
    
    pause(0.05);  
    
    frame_no(i) = getframe(gcf) ;
    writeVideo(writerObj, frame_no(i));
    drawnow
    
    xlim('manual')
    ylim('manual')
    zlim('manual')
    view(-37.5,30)
end

% close the writer object
close(writerObj);
